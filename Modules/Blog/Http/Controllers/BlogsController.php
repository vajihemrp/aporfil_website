<?php

namespace Modules\Blog\Http\Controllers;

use App\Helpers\ApiResponse;
use Illuminate\Http\Request;

use App\Http\Requests;
use Modules\Article\Repositories\ArticleRepository;
use Modules\Blog\Criteria\SearchoneCriteria;
use Modules\Blog\Criteria\SearchtwoCriteria;
use Modules\Blog\Criteria\SoftdeleteCriteria;
use Modules\Blog\Http\Requests\BlogCreateRequest;
use Modules\Blog\Http\Requests\BlogSearchRequest;
use Modules\Blog\Http\Requests\BlogUpdateRequest;
use Modules\Course\Repositories\CourseRepository;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Modules\Blog\Repositories\BlogRepository;
use App\Http\Controllers\Controller;
/**
 * Class BlogsController.
 *
 * @package namespace App\BlogHttp\Controllers;
 */
class BlogsController extends Controller
{
    /**
     * @var BlogRepository
     */
    protected $repository;

    /**
     * @var BlogValidator
     */
    protected $validator;

    protected $apiResponse;

    protected $articleRepository;

    /**
     * BlogsController constructor.
     *
     * @param BlogRepository $repository
     * @param BlogValidator $validator
     */
    public function __construct(BlogRepository $repository, ApiResponse $apiResponse,ArticleRepository $articleRepository)
    {
        $this->repository = $repository;
        $this->apiResponse = $apiResponse;
        $this->articleRepository = $articleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = $this->repository->scopeQuery(function ($q) {
            return $q->orderBy('id', 'desc');
        })->paginate();

        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($blogs, 'یافت شد', true);
        }

        return view('blog::index', compact('blogs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  BlogCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(BlogCreateRequest $request)
    {
        $blog = $this->repository->create($request->all());

        if ($request->wantsJson()) {

            return $this->apiResponse->sendResponse($blog, 'یا موفقیت ثبت شد', true);
        }

        return redirect()->back()->with('message', 'با موفقیت ثبت شد');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = $this->repository->find($id);

        if (request()->wantsJson()) {

            return $this->apiResponse->sendResponse($blog, 'یافت شد', true);
        }

        return view('blog::show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = $this->repository->find($id);

        return view('blog::edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BlogUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(BlogUpdateRequest $request, $id)
    {
        $blog = $this->repository->update($request->all(), $id);

        if ($request->wantsJson()) {

            return $this->apiResponse->sendResponse($blog, 'ویرایش شد', true);
        }

        return redirect()->back()->with('message', 'ویرایش شد');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = $this->repository->with(['articles'])->findWhere(['id'=>$id])->first();

        if (empty($blog)) {
            if (request()->wantsJson()) {
                $this->apiResponse->sendError('یافت نشد');
            }

            return redirect()->back()->with('error', 'یافت نشد');
        }

        $deleted = $this->repository->delete($id);

//        foreach ($blog->articles  as $article) {
//            $articleID = $article->id;
////            $this->articleRepository->update(['soft_delete'=>true],$articleID);
//            $this->articleRepository->update(['blog_id'=>null],$articleID);
//        }

        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($deleted, 'با موفقیت حذف شد', true);
        }

        return redirect()->back()->with('message',  'با موفقیت حذف شد');
    }

    public function search(BlogSearchRequest $request)
    {
        $this->repository->pushCriteria(new SearchoneCriteria($request->name,$request->status));

//        $this->repository->pushCriteria(new SoftdeleteCriteria());
        $blogs = $this->repository->orderBy('id','desc')->paginate();


        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($blogs, 'یافت شد', true);
        }

        return view('blog::index', compact('blogs'));
    }
}
