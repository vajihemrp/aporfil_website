<?php

namespace Modules\Blog\Http\Controllers;

use App\Helpers\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Blog\Repositories\BlogRepository;

class BlogController extends Controller
{
    protected $apiResponse;

    protected $repository;

    public function __construct(BlogRepository $repository, ApiResponse $apiResponse)
    {
        $this->repository = $repository;
        $this->apiResponse = $apiResponse;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
//        $items = $this->repository->findWhere(['soft_delete'=>'0','status'=>1])->all();
//
//        $blogs = $this->blogRepository->findWhere(['status' => 1,'soft_delete'=>0]);
//
//        $classes = $this->classifiactionRepository->findWhere(['status' => 1]);
//
//        $title = $blog->name;
//
//        $result = [];
//        $result['items'] = $items;
//        $request['blogs'] = $blogs;
//        $result['classes'] = $classes;
//        $result['title'] = $title;
//
////        return $items->items();
////        return empty($items) ? 'empty' : $items;
//
//        if (request()->wantsJson()) {
//            return $this->apiResponse->sendResponse($result, 'مقالات یافت شد', true);
//        }
//
//        return view('blog-list', compact('items', 'title', 'blogs', 'classes'));
    }

    public function articleList ($blog_id)
    {

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('blog::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('blog::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('blog::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
