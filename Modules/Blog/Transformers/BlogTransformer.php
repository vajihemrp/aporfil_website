<?php

namespace Modules\Blog\Transformers;

use League\Fractal\TransformerAbstract;
use Modules\Blog\Entities\Blog;

/**
 * Class BlogTransformer.
 *
 * @package namespace App\BlogTransformers;
 */
class BlogTransformer extends TransformerAbstract
{
    /**
     * Transform the Blog entity.
     *
     * @param \App\BlogEntities\Blog $model
     *
     * @return array
     */
    public function transform(Blog $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
