<?php

namespace Modules\Blog\Presenters;

use Modules\Blog\Transformers\BlogTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BlogPresenter.
 *
 * @package namespace App\BlogPresenters;
 */
class BlogPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BlogTransformer();
    }
}
