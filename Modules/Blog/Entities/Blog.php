<?php

namespace Modules\Blog\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Article\Entities\Article;
use Modules\Course\Entities\Course;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Blog.
 *
 * @package namespace App\BlogEntities;
 */
class Blog extends Model implements Transformable
{
    use TransformableTrait,Sluggable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','status','blog_id'];


    public function articles(){
        return $this->hasMany(Article::class,'blog_id');
    }

    public function getCreatedAtAttribute($value)
    {
        $datetimeArray = explode(" ", $value);
        $dateArray = explode("-", $datetimeArray['0'] );
        $dateArray = gregorian_to_jalali($dateArray[0], $dateArray[1], $dateArray[2], '/');
        return $dateArray." ".$datetimeArray[1];
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
