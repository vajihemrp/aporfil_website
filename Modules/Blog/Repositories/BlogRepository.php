<?php

namespace Modules\Blog\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BlogRepository.
 *
 * @package namespace App\BlogRepositories;
 */
interface BlogRepository extends RepositoryInterface
{
    //
}
