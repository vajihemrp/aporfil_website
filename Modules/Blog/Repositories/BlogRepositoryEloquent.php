<?php

namespace Modules\Blog\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Blog\Repositories\BlogRepository;
use Modules\Blog\Entities\Blog;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
/**
 * Class BlogRepositoryEloquent.
 *
 * @package namespace App\BlogRepositories;
 */
class BlogRepositoryEloquent extends BaseRepository implements BlogRepository,CacheableInterface
{
    use CacheableRepository;

    protected $fieldSearchable = [
        'name'=>'like',
        'created_at'=> 'like'
    ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Blog::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
