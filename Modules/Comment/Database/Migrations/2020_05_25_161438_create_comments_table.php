<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('content');
            $table->string('audio_file',191)->nullable();
            $table->string('video_file',191)->nullable();
            $table->string('image_file',191)->nullable();
            $table->bigInteger('course_id')->unsigned()->index()->nullable();
            $table->bigInteger('user_id')->unsigned()->index();
            $table->boolean('status')->default(1);
            $table->timestamps();

             $table->foreign('course_id')->references('id')
                 ->on('courses')
                 ->onDelete('cascade')->onUpdate('cascade');

             $table->foreign('user_id')->references('id')
                 ->on('users')
                 ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
