<?php

namespace Modules\Comment\Transformers;

use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;
use Modules\Comment\Entities\Comment;
use Storage;

/**
 * Class CommentTransformer.
 *
 * @package namespace Modules\Comment\Transformers;
 */
class CommentTransformer extends TransformerAbstract
{
    /**
     * Transform the Comment entity.
     *
     * @param \Modules\Comment\Entities\Comment $model
     *
     * @return array
     */
    public function transform(Comment $model)
    {
        return [
            'id'         => (int) $model->id,
            'content' => $model->content,
            /* place your other model properties here */
            'image_file' => $model->image_file != null ? Storage::disk('public')->url($model->image_file) : null,
            'audio_file' => $model->audio_file != null ? Storage::disk('public')->url($model->audio_file) : null,
            'video_file' => $model->video_file != null ? Storage::disk('public')->url($model->video_file) : null,
            'responses' => $this->responses($model),
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }

    /**
     * Get all responses.
     *
     * @param Model $model
     * @return array
     */
    protected function responses(Model $model)
    {
        $responses = [];
        foreach ($model->responses as $response) {
            $responses[] = [
                'id' => $response->id,
                'replay' => $response->replay,
                'status' => $response->status

            ];
        }

        return $responses;
    }
}
