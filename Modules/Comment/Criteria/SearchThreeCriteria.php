<?php

namespace Modules\Comment\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchThreeCriteria.
 *
 * @package namespace Modules\Comment\Criteria;
 */
class SearchThreeCriteria implements CriteriaInterface
{
    protected $content;

    protected $reply;

    protected $status;

    protected $column;

    protected $columnvalue;

    public function __construct($content,$reply,$status,$column,$columnvalue)
    {
        $this->reply = $reply;
        $this->content = $content;
        $this->status = $status;
        $this->column = $column;
        $this->columnvalue = $columnvalue;
    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (is_null($this->status)) {
            $condition = [
                ['content' , 'like' , '%'.$this->content.'%'],
                [$this->column , '=' , $this->columnvalue],
            ];

        }else{
            $condition = [
                ['content' , 'like' , '%'.$this->content.'%'],
                ['status','=',$this->status],
                [$this->column , '=' , $this->columnvalue],
            ];
        }

        if(!is_null($this->reply)){
            return  $model->whereHas('responses',function ($q) {
                $q->where('reply', 'like', '%'.$this->reply.'%');
            })->where($condition);
        }
        return  $model->where($condition);
    }
}
