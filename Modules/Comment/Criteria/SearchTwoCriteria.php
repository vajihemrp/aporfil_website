<?php

namespace Modules\Comment\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchTwoCriteria.
 *
 * @package namespace Modules\Comment\Criteria;
 */
class SearchTwoCriteria implements CriteriaInterface
{
    protected $content;

    protected $reply;

    protected $status;

    protected $column1;

    protected $columnvalue1;

    protected $column2;

    protected $columnvalue2;

    public function __construct($content,$reply,$status,$column1,$columnvalue1,$column2,$columnvalue2)
    {
        $this->reply = $reply;
        $this->content = $content;
        $this->status = $status;
        $this->column1 = $column1;
        $this->columnvalue1 = $columnvalue1;
        $this->column2 = $column2;
        $this->columnvalue2 = $columnvalue2;
    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (is_null($this->status)) {
            $condition = [
                ['content' , 'like' , '%'.$this->content.'%'],
                [$this->column1 , '=' , $this->columnvalue1],
                [$this->column2 , '=' , $this->columnvalue2],
            ];

        }else{
            $condition = [
                ['content' , 'like' , '%'.$this->content.'%'],
                ['status','=',$this->status],
                [$this->column1 , '=' , $this->columnvalue1],
                [$this->column2 , '=' , $this->columnvalue2],
            ];
        }

        if(!is_null($this->reply)){
            return  $model->whereHas('responses',function ($q) {
                $q->where('reply', 'like', '%'.$this->reply.'%');
            })->where($condition);
        }
        return  $model->where($condition);
    }
}
