<?php

namespace Modules\Comment\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchCriteria.
 *
 * @package namespace Modules\Comment\Criteria;
 */
class SearchCriteria implements CriteriaInterface
{
    protected $content;

    protected $reply;

    protected $status;

    public function __construct($content,$reply,$status)
    {
        $this->reply = $reply;
        $this->content = $content;
        $this->status = $status;
    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (is_null($this->status)) {
            $condition = [
                ['content' , 'like' , '%'.$this->content.'%'],
            ];

        }else{
            $condition = [
                ['content' , 'like' , '%'.$this->content.'%'],
                ['status','=',$this->status],
            ];
        }

        if(!is_null($this->reply)){
            return  $model->whereHas('responses',function ($q) {
                $q->where('reply', 'like', '%'.$this->reply.'%');
            })->where($condition);
        }
        return  $model->where($condition);
    }
}
