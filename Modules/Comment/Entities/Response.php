<?php

namespace Modules\Comment\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Response.
 *
 * @package namespace Modules\Comment\Entities;
 */
class Response extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['reply','comment_id','status'];

    public function comment()
    {
        return $this->belongsTo(Comment::class,'comment_id');
    }

    public function getCreatedAtAttribute($value)
    {
        $datetimeArray = explode(" ", $value);
        $dateArray = explode("-", $datetimeArray['0'] );
        $dateArray = gregorian_to_jalali($dateArray[0], $dateArray[1], $dateArray[2], '/');
        return $dateArray;
    }

    // public function getUpdatedAtAttribute($value)
    // {
    //     $datetimeArray = explode(" ", $value);
    //     $dateArray = explode("-", $datetimeArray['0'] );
    //     $dateArray = gregorian_to_jalali($dateArray[0], $dateArray[1], $dateArray[2], '/');
    //     return $dateArray." ".$datetimeArray[1];
    // }

}
