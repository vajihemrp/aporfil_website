<?php

namespace Modules\Comment\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Article\Entities\Article;
use Modules\Course\Entities\Course;
use Modules\Person\Entities\Customer;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Storage;

/**
 * Class Comment.
 *
 * @package namespace Modules\Comment\Entities;
 */
class Comment extends Model implements Transformable
{
    use TransformableTrait,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['content','audio_file','video_file','image_file','course_id','user_id','article_id','status','is_confirm','level'];

    protected $with = ['user'];

    public function responses()
    {
        return $this->hasOne(Response::class,'comment_id');
    }

    public function getImageFileAttribute($value)
    {
        if ($value) {
            //return Storage::disk('public')->url($value);
            return asset('storage/'.$value);
        }
        return null;
    }

    public function getVideoFileAttribute($value)
    {
        if ($value) {
           // return Storage::disk('public')->url($value);
            return asset('storage/'.$value);
        }
        return null;
    }

    public function getAudioFileAttribute($value)
    {
        if ($value) {
            //return Storage::disk('public')->url($value);
            return asset('storage/'.$value);
        }
        return null;
    }

    public function courses()
    {
        return $this->belongsTo(Course::class,'course_id');
    }

    public function getCreatedAtAttribute($value)
    {
        $datetimeArray = explode(" ", $value);
        $dateArray = explode("-", $datetimeArray['0'] );
        $dateArray = gregorian_to_jalali($dateArray[0], $dateArray[1], $dateArray[2], '/');
        return $dateArray;
    }

//    public function getUpdatedAtAttribute($value)
//    {
//        $datetimeArray = explode(" ", $value);
//        $dateArray = explode("-", $datetimeArray['0'] );
//        $dateArray = gregorian_to_jalali($dateArray[0], $dateArray[1], $dateArray[2], '/');
//        return $dateArray." ".$datetimeArray[1];
//    }

    public function user()
    {
        return $this->belongsTo(Customer::class,'user_id');
    }

    public function articles()
    {
        return $this->belongsTo(Article::class,'article_id');
    }
}
