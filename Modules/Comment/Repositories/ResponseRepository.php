<?php

namespace Modules\Comment\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ResponseRepository.
 *
 * @package namespace Modules\Comment\Repositories;
 */
interface ResponseRepository extends RepositoryInterface
{
    //
}
