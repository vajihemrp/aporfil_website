<?php

namespace Modules\Comment\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CommentRepository.
 *
 * @package namespace Modules\Comment\Repositories;
 */
interface CommentRepository extends RepositoryInterface
{
    //
}
