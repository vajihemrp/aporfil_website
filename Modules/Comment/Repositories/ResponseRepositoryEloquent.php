<?php

namespace Modules\Comment\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Comment\Repositories\ResponseRepository;
use Modules\Comment\Entities\Response;
use Modules\Comment\Validators\ResponseValidator;

/**
 * Class ResponseRepositoryEloquent.
 *
 * @package namespace Modules\Comment\Repositories;
 */
class ResponseRepositoryEloquent extends BaseRepository implements ResponseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Response::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
