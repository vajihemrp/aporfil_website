<?php

namespace Modules\Comment\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Comment\Repositories\CommentRepository;
use Modules\Comment\Entities\Comment;
use Modules\Comment\Validators\CommentValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
/**
 * Class CommentRepositoryEloquent.
 *
 * @package namespace Modules\Comment\Repositories;
 */
class CommentRepositoryEloquent extends BaseRepository implements CommentRepository,CacheableInterface
{
    use CacheableRepository;

    protected $fieldSearchable = [
        'content'=>'like',
        'created_at'=>'like',
        'responses.reply'=>'like'
    ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Comment::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
