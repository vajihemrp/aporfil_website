<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth:admin')->prefix('admin')->group(function() {
    Route::resource('/comments', 'CommentsController');
    Route::get('/deleteAttach/comments/{id}/{type}','CommentsController@deleteAttach');
    Route::post('/response/{response}', 'ResponseController@store');
});

Route::middleware('auth:web')->prefix('website')->group(function() {
    Route::get('/comments', 'CommentController@index');
    Route::post('/store/comment', 'CommentsController@store');
});