<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/comment', function (Request $request) {
//    return $request->user();
//});

Route::middleware('auth:api-admin')->prefix('admin')->group(function() {
    Route::resource('/comments', 'CommentsController');
    Route::post('/store/comment', 'CommentsController@store');
    Route::get('/deleteAttach/{id}/{type}','CommentsController@deleteAttach');
    Route::post('/response', 'ResponseController@store');
    Route::put('/response/{comment}', 'ResponseController@update');

    Route::get('list/comments', 'CommentsController@list');
});

Route::middleware('auth:api')->prefix('website')->group(function() {
    Route::get('/comments', 'CommentController@index');
});