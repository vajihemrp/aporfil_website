<?php

namespace Modules\Comment\Http\Controllers;

use App\Helpers\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Comment\Repositories\CommentRepository;

class CommentController extends Controller
{
    protected $apiResponse;
    protected $repository;
    public function __construct(ApiResponse $apiResponse,CommentRepository $repository)
    {
        $this->repository = $repository;
        $this->apiResponse = $apiResponse;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $comments = $this->repository->with(['user:id,name', 'courses:id,title'])->findWhere(['status' => 1]);

        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($comments, 'نظرات یافت شد', true);
        }
        return view('comment::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('comment::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('comment::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('comment::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
