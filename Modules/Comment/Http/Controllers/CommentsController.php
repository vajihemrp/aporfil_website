<?php

namespace Modules\Comment\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use App\Helpers\File;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Modules\Comment\Criteria\SearchCriteria;
use Modules\Comment\Criteria\SearchThreeCriteria;
use Modules\Comment\Criteria\SearchTwoCriteria;
use Modules\Comment\Http\Requests\CommentSearchRequest;
use Modules\Comment\Presenters\CommentPresenter;
use Modules\Comment\Repositories\ResponseRepository;
use Modules\Person\Repositories\CustomerRepository;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Modules\Comment\Http\Requests\CommentCreateRequest;
use Modules\Comment\Http\Requests\CommentUpdateRequest;
use Modules\Comment\Repositories\CommentRepository;
use App\Http\Controllers\Controller;

/**
 * Class CommentsController.
 *
 * @package namespace Modules\Comment\Http\Controllers;
 */
class CommentsController extends Controller
{
    /**
     * @var CommentRepository
     */
    protected $repository;

    /**
     * @var CommentValidator
     */
    protected $validator;

    protected $apiResponse;

    protected $responseRepository;

    protected $customerRepository;

    /**
     * CommentsController constructor.
     *
     * @param CommentRepository $repository
     * @param CommentValidator $validator
     */
    public function __construct(CommentRepository $repository,ApiResponse $apiResponse,
                                ResponseRepository $responseRepository,CustomerRepository $customerRepository)
    {
        $this->repository = $repository;
        $this->apiResponse = $apiResponse;
        $this->responseRepository = $responseRepository;
        $this->customerRepository = $customerRepository;

//        $this->middleware('auth:web')->only('store', 'insertCourseComment');
//        $this->middleware('auth:admin')->except('store', 'insertCourseComment');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $this->repository->setPresenter(CommentPresenter::class);
        $comments = $this->repository->with(['responses','courses','articles','user'])->orderBy('id','desc')->paginate();

        if (request()->wantsJson()) {

            return $this->apiResponse->sendResponse($comments, 'جلسات یافت شد', true);
        }

        return view('comment::index', compact('comments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CommentCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CommentCreateRequest $request)
    {
//      $this->repository->setPresenter(CommentPresenter::class);
        $user = \Auth::guard('web')->user();

        $data = $request->all();

        if (is_null($data['course_id']) && is_null($data['article_id'])) {
            if ($request->wantsJson()) {

                return $this->apiResponse->sendError('لطفا دوره یا مقاله ای انتخاب کنید');
            }

            return redirect()->back()->with('error', 'لطفا دوره یا مقاله ای انتخاب کنید');
        }

        if (!is_null($data['course_id']) && !is_null($data['article_id'])) {
            if ($request->wantsJson()) {

                return $this->apiResponse->sendError('لطفا دوره یا مقاله ای انتخاب کنید');
            }

            return redirect()->back()->with('error', 'لطفا دوره یا مقاله ای انتخاب کنید');
        }

        if (!is_null($data['course_id']) && is_null($data['article_id'])) {
            $check=$this->repository->findWhere(['user_id'=>$user->id,'course_id'=>$request->course_id])->first();

            if(!empty($check)){
                if ($request->wantsJson()) {

                    return $this->apiResponse->sendError('شما قبلا  برای این دوره یک نظر ثبت کرده اید');
                }

                return redirect()->back()->with('error', 'شما قبلا  برای این دوره یک نظر ثبت کرده اید');
            }

        }

        if (is_null($data['course_id']) && !is_null($data['article_id'])) {
            $check=$this->repository->findWhere(['user_id'=>$user->id,'article_id'=>$request->article_id])->first();

            if(!empty($check)){
                if ($request->wantsJson()) {

                    return $this->apiResponse->sendError('شما قبلا  برای این مقاله یک نظر ثبت کرده اید');
                }

                return redirect()->back()->with('error', 'شما قبلا  برای این مقاله یک نظر ثبت کرده اید');
            }

        }


        if($request->hasFile('image_file')){
            $image_path = File::imageUpload($request->image_file, 'comments/images');
            $data['image_file'] = $image_path;
        }

        if($request->hasFile('audio_file')){
            $audio_path = File::fileUpload($request->audio_file, 'comments/audio','public');
            $data['audio_file'] = $audio_path;
        }

        if($request->hasFile('video_file')){
            $video_path = File::fileUpload($request->video_file, 'comments/videos','public');
            $data['video_file'] = $video_path;
        }

//        $user = $this->customerRepository->findWhere(['email' => $request->email])->first();

        if (empty($user)) {
            if ($request->wantsJson()) {

                return $this->apiResponse->sendError('کاربری با این مشخصات یافت نشد');
            }

            return redirect()->back()->with('error', 'کاربری با این مشخصات یافت نشد');
        }

        $data['user_id'] = $user->id;
        $data['status']=0;

        $comment = $this->repository->create($data);

        $comment = $this->repository->with(['responses','courses','articles','user'])->findWhere(['id'=>$comment->id])->first();

        if ($request->wantsJson()) {

                return $this->apiResponse->sendResponse($comment, 'با موفقیت ثبت شد', true);
            }

        return redirect()->back()->with('message', 'با موفقیت ثبت شد');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['status']=1;
        $this->repository->update($data,$id);

        $comment = $this->repository->with(['responses','courses','articles','user'])->find($id);

        if (request()->wantsJson()) {

            return $this->apiResponse->sendResponse($comment, 'نظر یافت شد', true);
        }

        return view('comment::show', compact('comment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = $this->repository->find($id);

        return view('comment::edit', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CommentUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(CommentUpdateRequest $request, $id)
    {
//        $this->repository->setPresenter(CommentPresenter::class);

        if (is_null($request['course_id']) && is_null($request['article_id'])) {
            if ($request->wantsJson()) {

                return $this->apiResponse->sendError('لطفا دوره یا مقاله ای انتخاب کنید');
            }

            return redirect('admin/comments')->with('error', 'لطفا دوره یا مقاله ای انتخاب کنید');
        }

        if (!is_null($request['course_id']) && !is_null($request['article_id'])) {
            if ($request->wantsJson()) {

                return $this->apiResponse->sendError('لطفا دوره یا مقاله ای انتخاب کنید');
            }

            return redirect('admin/comments')->with('error', 'لطفا دوره یا مقاله ای انتخاب کنید');
        }

        $data = $request->except(['response','status']);

        if(!is_null($request['article_id'])){
            $data['level']=0;
        }

        if($request->hasFile('image_file')){
            $image_path = File::imageUpload($request->image_file, 'comments/images');
            $data['image_file'] = $image_path;
        }

        if($request->hasFile('audio_file')){
            $audio_path = File::fileUpload($request->audio_file, 'comments/audio','public');
            $data['audio_file'] = $audio_path;
        }

        if($request->hasFile('video_file')){
            $video_path = File::fileUpload($request->video_file, 'comments/videos','public');
            $data['video_file'] = $video_path;
        }

        $this->repository->update($data, $id);

        $comment = $this->repository->with(['responses','courses','articles','user'])->findWhere(['id'=>$id])->first();

        if(!is_null($request->reply)){
            $response_data['reply'] = $request->reply;
            $response_data['status'] = $request->status;

            if (is_null($comment->responses)) {
                $response_data['comment_id'] = $id;
                $this->responseRepository->create($response_data);
            }else{

                $this->responseRepository->update($response_data, $comment->responses->id);
            }

        }

        if ($request->wantsJson()) {

            return $this->apiResponse->sendResponse($comment, 'نظر با موفقیت ویزایش شد', true);
        }

        return redirect('admin/comments')->with('message', 'نظر با موفقیت ثبت شد');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = $this->repository->findwhere(['id'=>$id])->first();

        if ($comment) {
            $image= $comment->image_file;
            $video= $comment->video_file;
            $audio= $comment->audio_file;
        }else{
            if (request()->wantsJson()) {

                return $this->apiResponse->sendError( 'نظر یافت نشد');
            }

            return redirect('admin/comments')->with('error', 'نظر یافت نشد');
        }

        $deleted = $this->repository->delete($id);

        if ($deleted) {
            if ($image) {
                $link_array = explode('/',$image);
                $segment = count($link_array);
                $path = $link_array[$segment-3].'/'.$link_array[$segment-2].'/'.$link_array[$segment-1];
                File::delete($path);
            }
            if ($video) {
                $link_array = explode('/',$video);
                $segment = count($link_array);
                $path = $link_array[$segment-3].'/'.$link_array[$segment-2].'/'.$link_array[$segment-1];
                File::delete($path);
            }
            if ($audio) {
                $link_array = explode('/',$audio);
                $segment = count($link_array);
                $path = $link_array[$segment-3].'/'.$link_array[$segment-2].'/'.$link_array[$segment-1];
                File::delete($path);
            }
        }

        if (request()->wantsJson()) {

            return $this->apiResponse->sendResponse($comment, 'نظر با موفقیت حذف شد', true);
        }

        return redirect('admin/comments')->with('message', 'نظر با موفقیت حذف شد');
    }

    public function deleteAttach($id,$type)
    {
        $attache = $this->repository->find($id);
        if ($type == 'image') {
            $file = $attache->image_file;
            $data['image_file']=null;
        }
        if ($type=='audio'){
            $file = $attache->audio_file;
            $data['audio_file']=null;
        }

        if ($type == 'video') {
            $file = $attache->video_file;
            $data['video_file']=null;
        }

        File::delete($file);

        $this->repository->update($data, $id);

        if (request()->wantsJson()) {

            return $this->apiResponse->sendResponse( $attache, 'با موفقیت حذف شد', true);
        }

        return redirect()->back()->with('success', 'فایل حذف شد');
    }

    public function list(CommentSearchRequest $request)
    {
        if (!is_null($request->course_id) && is_null($request->user_id) && is_null($request->article_id)) {
            $this->repository->pushCriteria(new SearchThreeCriteria($request['content'],$request['reply'],$request['status'],
                'course_id',$request['course_id']));

        } elseif (is_null($request->course_id) && !is_null($request->user_id) && is_null($request->article_id)) {
            $this->repository->pushCriteria(new SearchThreeCriteria($request['content'], $request['reply'], $request['status'],
                'user_id', $request['user_id']));

        }elseif(is_null($request->course_id) && is_null($request->user_id) && !is_null($request->article_id)){
            $this->repository->pushCriteria(new SearchThreeCriteria($request['content'], $request['reply'], $request['status'],
                'article_id', $request['article_id']));

        } elseif (!is_null($request->course_id) && !is_null($request->user_id) && is_null($request->article_id)) {
            $this->repository->pushCriteria(new SearchTwoCriteria($request['content'],$request['reply'],$request['status'],
                'course_id',$request['course_id'],'user_id',$request['user_id']));

        } elseif (is_null($request->course_id) && is_null($request->user_id) && is_null($request->article_id)) {

            $this->repository->pushCriteria(new SearchCriteria($request['content'],$request['reply'],$request['status']));

        }elseif (!is_null($request->course_id) && is_null($request->user_id) && !is_null($request->article_id)) {

            $this->repository->pushCriteria(new SearchTwoCriteria($request['content'],$request['reply'],$request['status'],
                'article_id',$request['article_id'],'user_id',$request['user_id']));

        }elseif (is_null($request->course_id) && !is_null($request->user_id) && !is_null($request->article_id)) {

            $this->repository->pushCriteria(new SearchTwoCriteria($request['content'],$request['reply'],$request['status'],
                'course_id',$request['course_id'],'article_id',$request['article_id']));

        }

        $comments = $this->repository->with(['responses','courses','articles','user'])->orderBy('id','desc')->paginate();

        if (request()->wantsJson()) {

            return $this->apiResponse->sendResponse($comments, 'یافت شد', true);
        }

        return view('comment::index', compact('comments'));
    }

}
