<?php

namespace Modules\Comment\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use App\Helpers\File;

use App\Http\Requests;
use Modules\Comment\Http\Requests\ResponseCreateRequest;
use Modules\Comment\Http\Requests\ResponseUpdateRequest;
use Modules\Comment\Presenters\CommentPresenter;
use Modules\Comment\Repositories\CommentRepository;
use Modules\Comment\Repositories\ResponseRepository;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Controllers\Controller;

/**
 * Class CommentsController.
 *
 * @package namespace Modules\Comment\Http\Controllers;
 */
class ResponseController extends Controller
{
    /**
     * @var CommentRepository
     */
    protected $repository;

    /**
     * @var CommentValidator
     */
    protected $validator;

    protected $apiResponse;

    protected $commentRepository;

    protected $commentRepositoy;

    /**
     * CommentsController constructor.
     *
     * @param ResponseRepository $repository
     * @param ResponseValidator $validator
     */
    public function __construct(ResponseRepository $repository,ApiResponse $apiResponse,CommentRepository $commentRepository)
    {
        $this->repository = $repository;
        $this->apiResponse = $apiResponse;
        $this->commentRepository = $commentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ResponseCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ResponseCreateRequest $request)
    {
//        $data=$request->except(['comment']);
        $comment=$this->commentRepository->with(['responses'])->findWhere(['id'=>$request->comment_id])->first();

        if ($comment) {
            if (count($comment->responses)==0) {
                $this->repository->create($request->all());
                $comment=$this->commentRepository->with(['responses'])->find($request->comment_id);
                if ($request->wantsJson()) {

                    return $this->apiResponse->sendResponse($comment, 'با موفقیت ثبت شد', true);
                }

                return redirect('admin/comments')->with('message', 'با موفقیت ثبت شد');
            }
            if ($request->wantsJson()) {

                return $this->apiResponse->sendError('پاسخ قبلا درج شده است');
            }

            return redirect('admin/comments')->with('error', 'پاسخ قبلا درج شده است');
        }
        if ($request->wantsJson()) {

            return $this->apiResponse->sendError('نظر یافت نشد');
        }

        return redirect('admin/comments')->with('error', 'نظر یافت نشد');


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ResponseUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ResponseUpdateRequest $request, $commentID)
    {
        $data=$request->all();
        $data['comment_id']=$commentID;

        $response=$this->repository->findWhere(['comment_id'=>$commentID])->first();

        $response = $this->repository->update($data, $response->id);

        if ($request->wantsJson()) {
            return $this->apiResponse->sendResponse($response, 'با موفقیت ویرایش شد');
        }

        return redirect()->back()->with('message', 'با موفقیت ویرایش شد');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
