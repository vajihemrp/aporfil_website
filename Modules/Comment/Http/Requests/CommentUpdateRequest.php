<?php

namespace Modules\Comment\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Helpers\ApiResponse;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class CommentUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content'=>'required|string',
            'image_file'=>'nullable|image|max:10000',
            'audio_file' =>'nullable|file|mimes:mpga,wav',
            'video_file' =>'nullable|file|mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4',
            'course_id'=>'nullable|numeric|exists:courses,id',
            'user_id' =>'required|numeric|exists:users,id',
            'article_id'=>'nullable|numeric|exists:articles,id',
            'level'=>'required|numeric',
            'reply'=>'nullable|string'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'is_confirm' => $this->is_confirm=='on' ? 1 : 0,
            'status'=> $this->status=='on' ? 1 : 0,
        ]);
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson())
        {
            $response = new ApiResponse();
            throw new HttpResponseException($response->sendError('اطلاعات ارسال شده نامعتبر است', $validator->errors(), 422));
        }else{
            throw (new ValidationException($validator))
                ->errorBag($this->errorBag)
                ->redirectTo('/admin/comments');
        }
    }
}
