<?php

namespace Modules\Comment\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Helpers\ApiResponse;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class CommentCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content'=>'required|string',
            'image_file'=>'nullable|image|max:10000',
            'audio_file' =>'nullable|file|mimes:mpga,wav',
            'video_file' =>'nullable|file|mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4',
            'course_id'=>'nullable|numeric|exists:courses,id',
//          'user_id' =>'required|numeric|exists:users,id',
//          'email'=> ['required', 'email:filter', 'max:255'],
            'article_id'=>'nullable|numeric|exists:articles,id',
            'level'=>'nullable|numeric'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

//    protected function prepareForValidation()
//    {
//        $this->merge([
//            'status' => $this->status ? 1 : 0
//        ]);
//    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson())
        {
            $response = new ApiResponse();
            throw new HttpResponseException($response->sendError('اطلاعات ارسال شده نامعتبر است', $validator->errors(), 422));
        }else{
            throw (new ValidationException($validator))
                ->errorBag($this->errorBag)
                ->redirectTo($this->getRedirectUrl());
        }
    }

    protected function getRedirectUrl()
    {
        $url = $this->redirector->getUrlGenerator();

        if ($this->redirect) {
            return $url->to($this->redirect);
        } elseif ($this->redirectRoute) {
            return $url->route($this->redirectRoute);
        } elseif ($this->redirectAction) {
            return $url->action($this->redirectAction);
        }

        return $url->previous();
    }
}
