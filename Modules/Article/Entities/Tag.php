<?php

namespace Modules\Article\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
/**
 * Class Article.
 *
 * @package namespace Modules\Article\Entities;
 */
class Tag extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','article_id'];


    public function articles()
    {
        return $this->belongsTo(Article::class);
    }
}
