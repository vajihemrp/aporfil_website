<?php

namespace Modules\Article\Entities;

use App\Writer;
use Illuminate\Database\Eloquent\Model;
use Modules\Blog\Entities\Blog;
use Modules\Category\Entities\Category;
use Modules\Comment\Entities\Comment;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
/**
 * Class Article.
 *
 * @package namespace Modules\Article\Entities;
 */
class Article extends Model implements Transformable
{
    use TransformableTrait,Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','content','status','slug','writer_id','blog_id','soft_delete','image','visit'];

    protected $with = ['writer','tags','categories','blogs','comments'];


    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function categories()
    {
        return $this->morphToMany(Category::class,'categorizable');
    }

    public function writer()
    {
        return $this->belongsTo(Writer::class,'writer_id');
    }

    public function tags()
    {
        return $this->hasMany(Tag::class,'article_id');
    }

    public function getCreatedAtAttribute($value)
    {
        $datetimeArray = explode(" ", $value);
        $dateArray = explode("-", $datetimeArray['0'] );
        $dateArray = gregorian_to_jalali($dateArray[0], $dateArray[1], $dateArray[2], '/');
        return $dateArray;
    }

    public function getUpdatedAtAttribute($value)
    {
        $datetimeArray = explode(" ", $value);
        $dateArray = explode("-", $datetimeArray['0'] );
        $dateArray = gregorian_to_jalali($dateArray[0], $dateArray[1], $dateArray[2], '/');
        return $dateArray;
    }


    public function blogs()
    {
        return $this->belongsTo(Blog::class,'blog_id');
    }

    public function getImageAttribute($value)
    {
        if ($value) {

            return asset('storage/'.$value);
        }
        return null;
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'article_id');
    }
}
