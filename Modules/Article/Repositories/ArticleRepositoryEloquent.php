<?php

namespace Modules\Article\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Article\Repositories\ArticleRepository;
use Modules\Article\Entities\Article;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class ArticleRepositoryEloquent.
 *
 * @package namespace Modules\Article\Repositories;
 */
class ArticleRepositoryEloquent extends BaseRepository implements ArticleRepository,CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title'=>'like',
        'content'=>'like',
        'writers.name'=>'like',
        'tags.title'=>'like',
        'categories.name' => 'like'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Article::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
