<?php

namespace Modules\Article\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ArticleRepository.
 *
 * @package namespace Modules\Article\Repositories;
 */
interface ArticleRepository extends RepositoryInterface
{
    //
}
