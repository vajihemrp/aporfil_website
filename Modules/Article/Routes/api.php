<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/article', function (Request $request) {
//    return $request->user();
//});
Route::middleware('auth:api-admin')->prefix('admin')->middleware('auth:api-admin')->group(function() {
    Route::resource('/articles', 'ArticlesController');
    Route::get('list/articles', 'ArticlesController@list');
});

Route::prefix('website')->group(function() {
    Route::get('/articles', 'ArticleWebController@index');
    Route::get('articles/{slug}', 'ArticleWebController@show');
    Route::post('search/articles', 'ArticleWebController@search');
});