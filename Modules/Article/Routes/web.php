<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::prefix('article')->group(function() {
//    Route::get('/', 'ArticleController@index');
//});

Route::prefix('admin')->group(function() {
    Route::resource('/articles', 'ArticlesController');
});

Route::prefix('/website')->group(function() {
    Route::get('articles', 'ArticleWebController@index');
    Route::get('blog/class/{classSlug}/{slug}', 'ArticleWebController@show');
    Route::get('blog/language/{languageSlug}/{slug}', 'ArticleWebController@show');
    Route::post('search/articles', 'ArticleWebController@search');
//    Route::get('/list/articles/{blogSlug}', 'ArticleWebController@articleList');
    Route::get('/list/blogs/{blogSlug}', 'ArticleWebController@articleList');
    Route::get('article/category/{blogSlug}', 'ArticleWebController@CategoryArticle');
    Route::get('blog/category/{blogSlug}', 'ArticleWebController@CategoryArticle');
});