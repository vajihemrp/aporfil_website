<?php

namespace Modules\Article\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleSearchRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if(!is_null($this->status)){
            $this->merge([
                'status' => $this->status ? $this->status : 0,
                'parent'=> $this->parent ? $this->parent : 0,
//                'blog_id'=> $this->blog_id ? $this->blog_id : "",
//                'writer_id' => $this->writer_id ? $this->writer_id : ""
            ]);
        }else{
            $this->merge([
                'parent'=> $this->parent ? $this->parent : 0,
//                'blog_id'=> $this->blog_id ? $this->blog_id : "",
//                'writer_id' => $this->writer_id ? $this->writer_id : ""
            ]);
        }

    }
}
