<?php

namespace Modules\Article\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Helpers\ApiResponse;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class ArticleUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id.*' => 'required|numeric',
            'tags'    => 'required|array',
            'tags.*' => 'required|string',
            'writer_id'=>'exists:writers,id',
            'title'=>'required|string',
            'content'=>'required|string',
            'blog_id'=>'required|exists:blogs,id',
            'image'=>'nullable|mimes:jpeg,bmp,png',
            'tongue_id'=>'required|exists:tongues,id',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'status' => $this->status=='on' ? 1 : 0
        ]);
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson())
        {
            $response = new ApiResponse();
            throw new HttpResponseException($response->sendError('اطلاعات ارسال شده نامعتبر است', $validator->errors(), 422));
        }else{
            throw (new ValidationException($validator))
                ->errorBag($this->errorBag)
                ->redirectTo('/admin/articles');
        }
    }
}
