<?php

namespace Modules\Article\Http\Controllers;

use App\Helpers\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Article\Criteria\SearchWebCriteria;
use Modules\Article\Criteria\SoftDeleteCriteriaCriteria;
use Modules\Article\Http\Requests\ArticleSearchRequest;
use Modules\Article\Repositories\ArticleRepository;
use Modules\Article\Repositories\TagRepository;
use Modules\Blog\Repositories\BlogRepository;
use Modules\Category\Repositories\CategoryRepository;
use Modules\Comment\Repositories\CommentRepository;
use Modules\Article\Criteria\MyCriteria;

use Illuminate\Container\Container;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class ArticleWebController extends Controller
{
    protected $repository;
    protected $apiResponse;
    protected $blogRepository;
    protected $classificationRepository;
    protected $title;
    protected $settingRepository;
    protected $tagRepository;
    protected $commentRepository;
    protected $categoryRepository;

    public function __construct(
        ArticleRepository $repository,
        ApiResponse $apiResponse,
        BlogRepository $blogRepository,
        TagRepository $tagRepository,
        CommentRepository $commentRepository,
        CategoryRepository $categoryRepository
    )
    {
        $this->repository = $repository;
        $this->apiResponse = $apiResponse;
        $this->blogRepository = $blogRepository;
        $this->tagRepository = $tagRepository;
        $this->commentRepository = $commentRepository;
        $this->categoryRepository = $categoryRepository;
        $this->title = 'مقالات';
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $items = $this->repository->whereHas('blogs', function ($q) {
            return $q->where([['status', '=', 1]]);
        })->whereHas('categories', function ($query) {
            return $query->where('status', '=', 1);
        })->where(['status' => 1])->paginate();

        $blogs = $this->blogRepository->findWhere(['status' => 1]);

        $this->categoryRepository->pushCriteria(new MyCriteria('status', 1));

        $categories = $this->categoryRepository->all();  // دسته بندی محصولات

        $title = 'بلاگ';

        $result = [];
        $result['items'] = $items;
        $result['blogs'] = $blogs;
        $result['categories'] = $categories;
        $result['title'] = $title;

        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($result, 'مقالات یافت شد', true);
        }

        return view('blog-list', compact('items', 'title', 'blogs', 'categories'));

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('article::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($groupSlug,$slug)
    {
        $article = $this->repository->with(['categories'=>function($q){
            return $q->where('status', 1);
        }, 'writer:id,name', 'tags:id,title,article_id', 'comments', 'blogs'])
            ->findWhere(['slug' => $slug])
            ->first();

        $visit = (int)$article->visit;
        $article = $this->repository->update(['visit' => $visit+1], $article->id);

        $most_visited_article = $this->repository->scopeQuery(function ($query) {
            return $query->orderBy('visit', 'desc');
        })->get()->take(3);

        $blog_categories_count = $this->blogRepository->withCount('articles')->where([['status','=',1]])->get();

        $active_articles = $this->repository->scopeQuery(function ($query) {
            return $query->orderBy('id', 'desc');
        })->findWhere(['blog_id' => $article->blog_id,'status'=> 1 ]);

        $array_id = array();
        foreach ($active_articles as $active) {
            if($active->id != $article->id){
                array_push($array_id, $active->id);
            }
        }

        $blogs = $this->blogRepository->findWhere(['status' => 1]);

        $blog = $this->blogRepository->findWhere(['id' => $article->blog_id])->first();

        $this->categoryRepository->pushCriteria(new MyCriteria('status', 1));

        $categories = $this->categoryRepository->all();

        $tags = $this->tagRepository->select('title', \DB::raw('count(*) as total'))
            ->groupBy('title')
            ->orderBy('total','desc')->limit(6)->get();

        if(count($array_id)>1){
            $item = array_rand($array_id);
            $perv_article = $this->repository->with(['blogs'])->findWhere(['id' => $array_id[$item]])->first();
            $perv_article2 = $active_articles->filter(function ($row) use ($item) {
                return $row->id == $item ;
            })->first();

            if (($key = array_search($item, $array_id)) !== false) {
                unset($array_id[$key]);
            }
            $item = array_rand($array_id);
            $next_article = $this->repository->with(['blogs'])->findWhere(['id' => $array_id[$item]])->first();

//            if ($perv_article->id == $next_article->id) {
//                $item = array_rand($array_id);
//                $next_article = $this->repository->findWhere(['id' => $array_id[$item]])->first();
//            }
        }elseif(count($array_id)==1){
            $item = array_rand($array_id);
            $perv_article = $this->repository->with(['blogs'])->findWhere(['id' => $array_id[$item]])->first();

            $next_article=[];

        }else{
            $perv_article=[];
            $next_article=[];
        }

        $comments = $this->commentRepository->with(['user','responses'])->findWhere(['article_id' => $article->id,'is_confirm'=>1,'status'=>1]);

        $path= url()->current();
        $link_array = explode('/',$path);
        $this->type=$link_array[5];

        if( $this->type =='language' ){
            $cat = $this->categoryRepository->findWhere(['slug'=>$groupSlug])->first();
            $language= $cat->name;
            $languageSlug= $groupSlug;
        }else{
            $language = null;
            $languageSlug= null;
        }

        $title= $article->title;

        $result = [];
        $result ['article'] = $article;
        $result ['most_visited_article'] = $most_visited_article;
        $result ['blog_categories_count'] = $blog_categories_count;
        $result ['perv_article'] = $perv_article;
        $result ['next_article'] = $next_article;
        $result ['blogs'] = $blogs;
        $result ['blog'] = $blog;
        $result ['tags'] = $tags;
        $result['comments']= $comments;
        $result['category'] = $language;
        $result['categorySlug'] = $languageSlug;
        $result ['title'] = $title;

        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($result, 'مقاله یافت شد', true);
        }

        return view('single-blog', compact(
            'article',
            'most_visited_article',
            'blog_categories_count',
            'perv_article',
            'perv_article2',
            'next_article',
            'categories',
            'blogs',
            'blog',
            'tags',
            'comments',
            'language',
            'languageSlug',
            'title'
        ));

    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('article::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(ArticleSearchRequest $request)
    {
        $data = [];
        $items=[];
        $data['title'] = $request->article;
        $data['content'] = $request->article;
        $data['status'] = 1;
        $article = $this->blogRepository->findWhere([['name', 'like', '%مقالات%'],['status','=',1],['soft_delete','=',0]])->first();

        if(empty($article)){
            $items = [];
        }else{
            $data['blog_id'] = $article->id;


            $this->repository->pushCriteria(new SearchWebCriteria($data['title'], $data['content'], $data['blog_id']));

            $this->repository->pushCriteria(new SoftDeleteCriteriaCriteria());

            $items = $this->repository->with(['categories', 'writer', 'tags', 'blogs'])->paginate();
        }


        $blogs = $this->blogRepository->findWhere(['status' => 1,'soft_delete'=>0]);

        $this->categoryRepository->pushCriteria(new MyCriteria('status', 1));

        $categories = $this->categoryRepository->all();

        $language=null;

        $result = [];
        $result['items'] = $items;
        $request['blogs'] = $blogs;
        $result['categories'] = $categories;
        $result['title'] = $this->title;
        $request['language']= $language;

        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($result, 'مقالات یافت شد', true);
        }

        $title = $this->title;
        return view('blog-list', compact('items', 'title', 'blogs', 'categories','language'));
    }

    public function articleList($blogSlug)
    {
        $blog = $this->blogRepository->findWhere(['slug'=>$blogSlug])->first();

        $this->repository->pushCriteria(new SoftDeleteCriteriaCriteria());

        $items=$this->repository->where([['blog_id', $blog->id], ['status' , 1]])->paginate();
//       $items=$this->repository->findWhere(['blog_id'=> $blog->id, 'status' => 1]);

        $blogs = $this->blogRepository->findWhere(['status' => 1]);

        $this->categoryRepository->pushCriteria(new MyCriteria('status', 1));

        $categories = $this->categoryRepository->all();

        $title = $blog->name;

        $language = null;

        $result = [];
        $result['items'] = $items;
        $request['blogs'] = $blogs;
        $request['categories'] = $categories;
        $result['title'] = $title;
        $result['language'] = $language;

//        return $items->items();
//        return empty($items) ? 'empty' : $items;

        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($result, 'مقالات یافت شد', true);
        }

        return view('blog-list', compact('items', 'title', 'blogs', 'categories','language'));
    }

    public function CategoryArticle($categorySlug,$blogId)
    {
        $category = $this->categoryRepository->with(['articles'=> function ($q) use ($blogId) {
            return $q->where([['status','=',1],['blog_id','=', $blogId]]);
        }])->findWhere(['slug'=>$categorySlug])->first();

        $language = $category->name;
        $languageSlug = $categorySlug;

        $items = $this->paginate($category->articles,16);

        $blogs = $this->blogRepository->findWhere(['status' => 1]);

        $this->categoryRepository->pushCriteria(new MyCriteria('status', 1));

        $categories = $this->categoryRepository->all();

//        $title = $category->name;
        $title = 'بلاگ';

        $result = [];
        $result['items'] = $items;
        $request['blogs'] = $blogs;
        $result['categories'] = $categories;
        $result['title'] = $title;
        $result['language'] = $language;
        $result['languageSlug'] = $languageSlug;

        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($result, 'یافت شد', true);
        }
        return view('blog-list',compact('items', 'title', 'blogs', 'categories','language','languageSlug'));

    }

    public static function paginate(Collection $results, $pageSize)
    {
        $page = Paginator::resolveCurrentPage('page');

        $total = $results->count();

        return self::paginator($results->forPage($page, $pageSize), $total, $pageSize, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page',
        ]);

    }

    /**
     * Create a new length-aware paginator instance.
     *
     * @param  \Illuminate\Support\Collection $items
     * @param  int $total
     * @param  int $perPage
     * @param  int $currentPage
     * @param  array $options
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    protected static function paginator($items, $total, $perPage, $currentPage, $options)
    {
        return Container::getInstance()->makeWith(LengthAwarePaginator::class, compact(
            'items', 'total', 'perPage', 'currentPage', 'options'
        ));
    }

}
