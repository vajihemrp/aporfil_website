<?php

namespace Modules\Article\Http\Controllers;

use App\Helpers\ApiResponse;
use App\Helpers\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use Modules\Article\Criteria\MyCriteria;
use Modules\Article\Criteria\SearchCategoryCriteria;
use Modules\Article\Criteria\SearchCriteria;
use Modules\Article\Criteria\SearchFourCriteria;
use Modules\Article\Criteria\SearchoneCriteria;
use Modules\Article\Criteria\SearchThreeCriteria;
use Modules\Article\Criteria\SearchTwofieldCriteria;
use Modules\Article\Criteria\SoftDeleteCriteriaCriteria;
use Modules\Article\Entities\Tag;
use Modules\Article\Http\Requests\ArticleSearchRequest;
use Modules\Category\Repositories\CategoryRepository;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Modules\Article\Http\Requests\ArticleCreateRequest;
use Modules\Article\Http\Requests\ArticleUpdateRequest;
use Modules\Article\Repositories\ArticleRepository;

/**
 * Class ArticlesController.
 *
 * @package namespace Modules\Article\Http\Controllers;
 */
class ArticlesController extends Controller
{
    /**
     * @var ArticleRepository
     */
    protected $repository;

    /**
     * @var ArticleValidator
     */
    protected $validator;

    protected $apiResponse;

    protected $categoryRepository;

    /**
     * ArticlesController constructor.
     *
     * @param ArticleRepository $repository
     * @param ArticleValidator $validator
     */
    public function __construct(ArticleRepository $repository, ApiResponse $apiResponse, CategoryRepository $categoryRepository)
    {
        $this->repository = $repository;
        $this->apiResponse = $apiResponse;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = $this->repository->orderBy('id', 'desc')->paginate();

        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($articles, 'مقالات یافت شد', true);
        }

        return view('article::index', compact('articles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ArticleCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ArticleCreateRequest $request)
    {
        date_default_timezone_set("Asia/Tehran");
        $data = $request->except(['tags', 'image', 'category_id']);

        if ($request->hasFile('image')) {
            $image_path = File::fileUpload($request->image, 'articles/images','public');
            $data['image'] = $image_path;
        }

        $article = $this->repository->create($data);

        foreach ($request->tags as $tag) {
            if (!empty($tag)) {
                $article->tags()->create(['title' => $tag]);
            }
        }

        if (array_key_exists('category_id', $request)) {
            $categories = $request->category_id;
            foreach ($categories as $key => $value) {
                $category = $this->categoryRepository->findWhere(['id' => $value])->first();
                if (!empty($category)) {
                    $article->categories()->attach($value);
                }
            }
        }



        $article = $this->repository->findWhere(['id' => $article->id])->first();

        if ($request->wantsJson()) {
            return $this->apiResponse->sendResponse($article, 'مقالات با موفقیت ثبت شد', true);
        }

        return redirect()->back()->with('message', 'مقالات با موفقیت ثبت شد');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = $this->repository->with(['categories', 'writer', 'tags'])->find($id);

        if (request()->wantsJson()) {

            return $this->apiResponse->sendResponse($article, 'مقالات بافت شد', true);
        }

        return view('article::show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = $this->repository->find($id);

        return view('article::edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ArticleUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ArticleUpdateRequest $request, $id)
    {
        date_default_timezone_set("Asia/Tehran");
        $data = $request->except(['category_id', 'tags', 'image']);

        $article = $this->repository->find($id);

        $categories = $request->category_id;
        $new_categories = [];
        foreach ($categories as $key => $value) {
            $category = $this->categoryRepository->findWhere(['id' => $value])->first();
            if (!empty($category)) {
                array_push($new_categories, $value);
            }
        }
        $article->categories()->sync($new_categories);


        //Delete Old Contents
        $article->tags()->delete();

        foreach ($request->tags as $tag) {
            if (!empty($tag)) {
                $article->tags()->create([
                    'title' => $tag,
                ]);
            }
        }

        if ($request->hasFile('image')) {
            $image_path = File::fileUpload($request->image, 'articles/images','public');
            $data['image'] = $image_path;
            $row = $this->repository->find($id);
            if ($row->image) {
                $link_array = explode('/', $row->image);
                $segment = count($link_array);
                $path = $link_array[$segment - 3] . '/' . $link_array[$segment - 2] . '/' . $link_array[$segment - 1];

                File::Delete($path);
            }
        }

        $this->repository->update($data, $id);

        $article = $this->repository->findWhere(['id' => $id])->first();

        if ($request->wantsJson()) {

            return $this->apiResponse->sendResponse($article, 'مقالات با موفقیت ویرایش شد', true);
        }

        return redirect()->back()->with('message', 'مقالات با موفقیت ویرایش شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = $this->repository->find($id);
        $article->categories()->detach();
        $article->tags()->delete();

        $image = $article->image;

        if ($image) {
            $link_array = explode('/', $image);
            $segment = count($link_array);
            $path = $link_array[$segment - 3] . '/' . $link_array[$segment - 2] . '/' . $link_array[$segment - 1];

            File::Delete($path);
        }

        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($deleted, 'مقاله یا موفقیت حذف شد', true);
        }

        return redirect()->back()->with('message', 'مقاله یا موفقیت حذف شد');
    }

    public function list(ArticleSearchRequest $request)
    {
        if (is_null($request->blog_id) && is_null($request->tongue_id) && empty($request->category_id)) {
            $this->repository->pushCriteria(new SearchCriteria($request['title'], $request['content'], $request['status']));

        } elseif (!is_null($request->blog_id) && is_null($request->tongue_id) && empty($request->category_id)) {
            $this->repository->pushCriteria(new SearchoneCriteria($request['title'], $request['content'], $request['status'], 'blog_id', $request['blog_id']));

        } elseif (is_null($request->blog_id) && !is_null($request->tongue_id) && empty($request->category_id)) {
            $this->repository->pushCriteria(new SearchoneCriteria($request['title'], $request['content'], $request['status'], 'tongue_id', $request['tongue_id']));

        } elseif (is_null($request->blog_id) && is_null($request->tongue_id) && !empty($request->category_id)) {
            $this->repository->pushCriteria(new SearchCategoryCriteria($request['title'], $request['content'], $request['status'], $request['category_id']));
        }

        if (!is_null($request->blog_id) && !is_null($request->tongue_id) && empty($request->category_id)) {
            $this->repository->pushCriteria(new SearchTwofieldCriteria($request['title'], $request['content'], $request['status'], $request['blog_id'], $request['tongue_id']));

        } elseif (!is_null($request->blog_id) && is_null($request->tongue_id) && !empty($request->category_id)) {
            $this->repository->pushCriteria(new SearchThreeCriteria($request['title'], $request['content'], $request['status'], 'blog_id', $request['blog_id'], $request['category_id']));

        } elseif (is_null($request->blog_id) && !is_null($request->tongue_id) && !empty($request->category_id)) {
            $this->repository->pushCriteria(new SearchThreeCriteria($request['title'], $request['content'], $request['status'], 'tongue_id', $request['tongue_id'], $request['category_id']));

        } elseif (!is_null($request->blog_id) && !is_null($request->tongue_id) && !empty($request->category_id)) {
            $this->repository->pushCriteria(new SearchFourCriteria($request['title'], $request['content'], $request['status'], $request['blog_id'], $request['tongue_id'], $request['category_id']));

        }

        $this->repository->pushCriteria(new SoftDeleteCriteriaCriteria());
        $articles = $this->repository->with(['categories', 'writer', 'tags', 'blogs'])->orderBy('id', 'desc')->paginate();

        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($articles, 'مقالات یافت شد', true);
        }

        return view('article::index', compact('articles'));
    }
}
