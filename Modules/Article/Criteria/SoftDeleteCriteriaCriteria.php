<?php

namespace Modules\Article\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SoftDeleteCriteriaCriteria.
 *
 * @package namespace Modules\Article\Criteria;
 */
class SoftDeleteCriteriaCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('soft_delete',0)->orderBy('id', 'desc');
    }
}
