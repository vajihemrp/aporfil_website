<?php

namespace Modules\Article\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchWebCriteria.
 *
 * @package namespace Modules\Article\Criteria;
 */
class SearchWebCriteria implements CriteriaInterface
{
    protected $title;

    protected $content;

    protected $blog_id;

    public function __construct($title,$content,$blog_id)
    {
        $this->title = $title;
        $this->content = $content;
        $this->blog_id = $blog_id;
    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('title','like','%'.$this->title.'%')
            ->orWhere('content','like','%'.$this->content.'%')
            ->where('status','=',1)
            ->where('blog_id','=',$this->blog_id);
    }
}
