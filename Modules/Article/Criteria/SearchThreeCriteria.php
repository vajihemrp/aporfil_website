<?php

namespace Modules\Article\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchThreeCriteria.
 *
 * @package namespace Modules\Article\Criteria;
 */
class SearchThreeCriteria implements CriteriaInterface
{
    protected $title;

    protected $content;

    protected $status;

    protected $category_id;

    protected $column;

    protected $columnvalue;

    public function __construct($title,$content,$status,$column,$columnvalue,$category_id)
    {
        $this->title = $title;
        $this->content = $content;
        $this->status = $status;
        $this->category_id = $category_id;
        $this->column = $column;
        $this->columnvalue = $columnvalue;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if(is_null($this->status)){
            $condition = [
                ['title','like','%'.$this->title.'%'],
                ['content' , 'like' , '%'.$this->content.'%'],
                [$this->column,'=',$this->columnvalue]
            ];

        }else{
            $condition = [
                ['title','like','%'.$this->title.'%'],
                ['content' , 'like' , '%'.$this->content.'%'],
                [$this->column,'=',$this->columnvalue],
                ['status','=',$this->status]
            ];
        }

        return  $model->whereHas('categories', function($q)
        {
            $q->whereIn('category_id', $this->category_id);
        })->where($condition);
    }
}
