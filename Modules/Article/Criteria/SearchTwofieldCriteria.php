<?php

namespace Modules\Article\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchTwofieldCriteria.
 *
 * @package namespace Modules\Article\Criteria;
 */
class SearchTwofieldCriteria implements CriteriaInterface
{
    protected $blog_id;

    protected $writer_id;

    protected $title;

    protected $content;

    protected $status;

    public function __construct($title,$content,$status,$blog_id,$writer_id)
    {
        $this->title = $title;
        $this->content = $content;
        $this->status = $status;
        $this->blog_id = $blog_id;
        $this->writer_id = $writer_id;
    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
       if(is_null($this->status)){
           $condition = [
               ['title','like','%'.$this->title.'%'],
               ['content' , 'like' , '%'.$this->content.'%'],
               ['blog_id','=',$this->blog_id],
               ['writer_id','=',$this->writer_id]
           ];

       }else{
           $condition = [
               ['title','like','%'.$this->title.'%'],
               ['content' , 'like' , '%'.$this->content.'%'],
               ['blog_id','=',$this->blog_id],
               ['writer_id','=',$this->writer_id],
               ['status','=',$this->status]
           ];
       }
        return  $model->where($condition);
    }
}
