<?php

namespace Modules\Article\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchCategoryCriteria.
 *
 * @package namespace Modules\Article\Criteria;
 */
class SearchCategoryCriteria implements CriteriaInterface
{

    protected $title;

    protected $content;

    protected $status;

    protected $category_id;


    public function __construct($title,$content,$status,$category_id)
    {
        $this->title = $title;
        $this->content = $content;
        $this->status = $status;
        $this->category_id= $category_id;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if(is_null($this->status)){
            $condition = [
                ['title','like','%'.$this->title.'%'],
                ['content' , 'like' , '%'.$this->content.'%'],
            ];

        }else{
            $condition = [
                ['title','like','%'.$this->title.'%'],
                ['content' , 'like' , '%'.$this->content.'%'],
                ['status','=',$this->status]
            ];
        }

        return  $model->whereHas('categories', function($q)
                        {
                            $q->whereIn('category_id', $this->category_id);
                        })->where($condition);
    }
}
