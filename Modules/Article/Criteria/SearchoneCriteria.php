<?php

namespace Modules\Article\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchoneCriteria.
 *
 * @package namespace Modules\Article\Criteria;
 */
class SearchoneCriteria implements CriteriaInterface
{
    protected $column;

    protected $columnValue;

    protected $title;

    protected $content;

    protected $status;


    public function __construct($title,$content,$status,$column,$columnValue)
    {
        $this->title = $title;
        $this->content = $content;
        $this->status = $status;
        $this->column = $column;
        $this->columnValue = $columnValue;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if(is_null($this->status)){
            $condition = [
                ['title','like','%'.$this->title.'%'],
                ['content' , 'like' , '%'.$this->content.'%'],
                [$this->column,'=',$this->columnValue]
            ];

        }else{
            $condition = [
                ['title','like','%'.$this->title.'%'],
                ['content' , 'like' , '%'.$this->content.'%'],
                ['status','=',$this->status],
                [$this->column,'=',$this->columnValue]
            ];
        }

        return  $model->where($condition);
    }
}
