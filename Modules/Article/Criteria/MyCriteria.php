<?php

namespace Modules\Article\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class MyCriteria.
 *
 * @package namespace App\ArticleCriteria;
 */
class MyCriteria implements CriteriaInterface
{
    protected $column;

    protected $columnValue;

    public function __construct($column,$columnValue)
    {
        $this->column = $column;
        $this->columnValue = $columnValue;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where($this->column,$this->columnValue);
    }
}
