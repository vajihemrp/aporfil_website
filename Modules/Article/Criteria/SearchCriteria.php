<?php

namespace Modules\Article\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchCriteria.
 *
 * @package namespace Modules\Article\Criteria;
 */
class SearchCriteria implements CriteriaInterface
{
//'title','content','status','slug','writer_id','blog_id'
    protected $title;

    protected $content;

    protected $status;

    public function __construct($title,$content,$status)
    {
        $this->title = $title;
        $this->content = $content;
        $this->status = $status;
    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if(is_null($this->status)){
            $condition = [
                ['title','like','%'.$this->title.'%'],
                ['content' , 'like' , '%'.$this->content.'%'],
            ];

        }else{
            $condition = [
                ['title','like','%'.$this->title.'%'],
                ['content' , 'like' , '%'.$this->content.'%'],
                ['status','=',$this->status],
            ];
        }

        return  $model->where($condition);
    }
}
