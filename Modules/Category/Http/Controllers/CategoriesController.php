<?php

namespace Modules\Category\Http\Controllers;

use App\Helpers\File;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Modules\Blog\Criteria\SoftdeleteCriteria;
use Modules\Blog\Repositories\BlogRepository;
use Modules\Category\Criteria\SearchoneCriteria;
use Modules\Category\Criteria\SearchtwoCriteria;
use Modules\Category\Http\Requests\CategorySearchRequest;
use Modules\Course\Repositories\CourseRepository;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Modules\Category\Http\Requests\CategoryCreateRequest;
use Modules\Category\Http\Requests\CategoryUpdateRequest;
use Modules\Category\Repositories\CategoryRepository;
use App\Helpers\ApiResponse;
/**
 * Class CategoriesController.
 *
 * @package namespace App\Http\Controllers;
 */
class CategoriesController extends Controller
{
    /**
     * @var CategoryRepository
     */
    protected $repository;

    protected $courseRepository;

    protected $blogRepositroy;

    /**
     * CategoriesController constructor.
     *
     * @param CategoryRepository $repository
     */
    public function __construct(CategoryRepository $repository,ApiResponse $apiResponse,
                                CourseRepository $courseRepository,BlogRepository $blogRepository)
    {
        $this->repository = $repository;
        $this->apiResponse=$apiResponse;
        $this->courseRepository = $courseRepository;
        $this->blogRepositroy = $blogRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->repository->scopeQuery(function ($q) {
            return $q->orderBy('id', 'desc');
        })->paginate();

        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($categories, 'دسته بندی یافت شد', true);
        }

        return view('category::index', compact('categories'));
    }

    public function create()
    {
        return view('category::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CategoryCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CategoryCreateRequest $request)
    {
        $data=$request->except(['image','image_medium']);

        if ($request->hasFile('image')) {
//            $image_path = File::imageUpload($request->image, 'categories/icons');
            $image_path = File::svgUpload($request->image, 'categories/icons');
            $data['image'] = $image_path;
        }

        if ($request->hasFile('image_medium')) {
            $image_path = File::imageUpload($request->image_medium, 'categories/image_medium');
            $data['image_medium'] = $image_path;
        }

        $categories = $this->repository->create($data);

        if ($request->wantsJson()) {
            return $this->apiResponse->sendResponse($categories, 'با موفقیت ثبت شد');
        }

        return redirect('admin/categories')->with('message',$categories);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = $this->repository->find($id);

        if (request()->wantsJson()) {

            return $this->apiResponse->sendResponse($category, 'با موفقیت یافت شد');
        }

        return view('category::show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->repository->find($id);

        return view('category::edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CategoryUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(CategoryUpdateRequest $request, $id)
    {
        $data = $request->except(['image','image_medium']);

        $row=$this->repository->find($id);

        if($request->hasFile('image')){
//            $image_path = File::imageUpload($request->image, 'categories/images');
//            $image_path = File::svgUpload($request->image, 'categories/images');

            $image_path = File::svgUpload($request->image, 'categories/icons');
//            $image_path= $request->image->storeAs('categories/images', $imageName);

            $data['image'] = $image_path;

            if ($row->image) {
                $link_array = explode('/',$row->image);
                $segment = count($link_array);
                $path = $link_array[$segment-3].'/'.$link_array[$segment-2].'/'.$link_array[$segment-1];

                File::Delete($path);
            }
        }

        if($request->hasFile('image_medium')){
            $image_path = File::imageUpload($request->image_medium, 'categories/image_medium');

            $data['image_medium'] = $image_path;

            if ($row->image_medium) {
                $link_array = explode('/',$row->image_medium);
                $segment = count($link_array);
                $path = $link_array[$segment-3].'/'.$link_array[$segment-2].'/'.$link_array[$segment-1];

                File::Delete($path);
            }
        }

        $category = $this->repository->update($data, $id);

        if ($request->wantsJson()) {
            return $this->apiResponse->sendResponse($category, 'با موفقیت ویرایش شد');
        }

        return redirect()->back()->with('message', 'با موفقیت ویرایش شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category=$this->repository->find($id);

        if ($category->image) {
            $link_array = explode('/',$category->image);
            $segment = count($link_array);
            $path = $link_array[$segment-3].'/'.$link_array[$segment-2].'/'.$link_array[$segment-1];

            File::Delete($path);
        }

        if ($category->image_medium) {
            $link_array = explode('/',$category->image_medium);
            $segment = count($link_array);
            $path = $link_array[$segment-3].'/'.$link_array[$segment-2].'/'.$link_array[$segment-1];

            File::Delete($path);
        }

//        $category->courses()->detach();
//        $category->articles()->detach();

        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($deleted,'با موفقیت حذف شد');
        }

        return redirect('/admin/categories')->with('message', 'دسته بندی با موفقیت حذف شد');
    }

    public function changeStatus($id)
    {
        $category = $this->repository->find($id);

        $data['status']=$category['status'];
        if($data['status']){
            $data['status'] = 0;
        }else{
            $data['status'] = 1;
        }

        if($this->repository->update($data, $id)){
            if (request()->wantsJson()) {
                return $this->apiResponse->sendResponse($category,'با موفقیت حذف شد');
            }
            return redirect('/admin/categories')->with('success', 'با موفقیت ویرایش شد.');
        }else{
            if (request()->wantsJson()) {
                return $this->apiResponse->sendError('خطایی در ویرایش اطلاعات رخ داده است');
            }
            return redirect('/admin/categories')->with('error', 'خطایی در ویرایش اطلاعات رخ داده است');
        }

    }

    public function list(CategorySearchRequest $request)
    {

        $this->repository->pushCriteria(new SearchoneCriteria($request->name,$request->status));

//      $this->repository->pushCriteria(new SoftdeleteCriteria());
        $categories = $this->repository->orderBy('id','desc')->paginate();

        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($categories, 'یافت شد', true);
        }

        return view('category::index', compact('categories'));
    }
}
