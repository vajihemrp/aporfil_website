<?php

namespace Modules\Category\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
//use Illuminate\Routing\Controller;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Modules\Blog\Repositories\BlogRepository;
use Modules\Category\Http\Requests\CategoryCreateRequest;
use Modules\Category\Http\Requests\CategoryUpdateRequest;
use Modules\Category\Repositories\CategoryRepository;
use App\Helpers\ApiResponse;
use Modules\Classification\Repositories\ClassifiactionRepository;
use Modules\Setting\Repositories\SettingRepository;
use Modules\Site\Criteria\ConditionCriteria;

class CategoryController extends Controller
{
    protected $apiResponse;

    protected $repository;

    protected $courseRepository;

    protected $settingRepository;

    protected $blogRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * CategoryController constructor.
     * @param ApiResponse $apiResponse
     * @param CategoryRepository $repository
     * @param BlogRepository $blogRepository
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(ApiResponse $apiResponse, CategoryRepository $repository,
                                BlogRepository $blogRepository, CategoryRepository $categoryRepository)
   {
       $this->apiResponse = $apiResponse;
       $this->repository = $repository;
       $this->blogRepository = $blogRepository;
       $this->categoryRepository = $categoryRepository;
   }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $categories = $this->repository->findWhere(['status' => 1]);

        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($categories, 'دسته بندی یافت شد', true);
        }

        return view('courses-category', compact('categories'));

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('category::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $terms=collect();
        $category=$this->repository->findWhere(['id'=>$id])->first();

        if (empty($category)) {
            if (request()->wantsJson()) {
                return $this->apiResponse->sendError('دسته بندی وجود ندارد');
            }

            return view('category::show')->with('error','دسته بندی وجود ندارد');
        }

        $courses=$category->courses;
        foreach ($courses as $course) {
            if ($course['kind']=='term') {
                $terms->push($course);
            }
        }

        if (request()->wantsJson()) {
            return $this->apiResponse->sendResponse($terms, 'دوره یافت شد', true);
        }

        return view('category::show')->with('message','دوره یافت شد');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('category::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function categorizable_type($slug)
    {
        $category = $this->repository->withCount(['articles' => function($query) {
                return $query->where([['blog_id','=','1'],['status','=',1]]);
            },'articles as videos_count' => function($query) {
            return $query->where([['blog_id','=','2'],['status','=',1]]);
        },'galleries'=> function($query) {
            return $query->where([['status','=',1]]);
        }])->findWhere(['slug' => $slug])->first();

        $blogs = $this->blogRepository->findWhere(['status' => 1]);

        $this->categoryRepository->pushCriteria(new ConditionCriteria('status', 1));

        $categories = $this->categoryRepository->all();

        $title = $category->name;

        $lanquage = $category->name;

        $categoryId = $category->id;

        return view('categories', [
            'title'=>$title,
            'slug'=>$slug,
            'categoryId'=>$categoryId,
            'categories'=> $categories,
            'blogs'=>$blogs,
            'language' => $lanquage,
            'articles_count' => $category->articles_count,
            'videos_count' => $category->videos_count,
            'galleries_count' => $category->galleries_count
        ]);

    }
}
