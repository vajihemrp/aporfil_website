<?php

namespace Modules\Category\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Article\Entities\Article;
use Modules\Course\Entities\Course;
use Modules\Gallery\Entities\Gallery;
use Modules\Reserve\Entities\Reserve;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category.
 *
 * @package namespace App\Entities;
 */
class Category extends Model implements Transformable
{
    use TransformableTrait,Sluggable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * image column show svg icons
     */
    protected $fillable = ['name','status','image','color','image_medium'];

    protected $dates = ['deleted_at'];

    public function courses()
    {
        return $this->morphedByMany(Course::class, 'categorizable');
    }

    public function articles()
    {
        return $this->morphedByMany(Article::class, 'categorizable');
    }

    public function categoryCourse()
    {
        return $this->courses()->where('kind','=','term');
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getImageAttribute($value)
    {
        if ($value) {

            return asset('storage/'.$value);
        }
        return null;
    }

    public function getImageMediumAttribute($value)
    {
        if ($value) {

            return asset('storage/'.$value);
        }
        return null;
    }

    public function galleries()
    {
        return $this->morphedByMany(Gallery::class, 'categorizable');
    }

    public function reservations()
    {
        return $this->hasMany(Reserve::class, 'category_id');
    }
}
