<?php

namespace Modules\Category\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchtwoCriteria.
 *
 * @package namespace Modules\Category\Criteria;
 */
class SearchtwoCriteria implements CriteriaInterface
{
    protected $column1;

    protected $columnValue1;

    protected $column2;

    protected $columnValue2;

    public function __construct($column1,$columnValue1,$column2,$columnValue2)
    {
        $this->column1 = $column1;
        $this->columnValue1 = $columnValue1;
        $this->column2 = $column2;
        $this->columnValue2 = $columnValue2;
    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $condition = [
            [$this->column1,'like','%'.$this->columnValue1.'%'],
            [$this->column2 , 'like' , '%'.$this->columnValue2.'%']
        ];
        return  $model->where($condition);
    }
}
