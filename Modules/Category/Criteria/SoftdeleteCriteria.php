<?php

namespace Modules\Category\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SoftdeleteCriteria.
 *
 * @package namespace Modules\Category\Criteria;
 */
class SoftdeleteCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('soft_delete',0);
    }
}
