<?php

namespace Modules\Category\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchoneCriteria.
 *
 * @package namespace Modules\Category\Criteria;
 */
class SearchoneCriteria implements CriteriaInterface
{
    protected $name;

    protected $status;

    public function __construct($name,$status)
    {
        $this->name = $name;
        $this->status = $status;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if(is_null($this->status)){
            $condition=[['name','like','%'.$this->name.'%']];
        }else{
            $condition=[
                ['name','like','%'.$this->name.'%'],
                ['status','=', $this->status]
            ];
        }
        return $model->where($condition);
    }
}
