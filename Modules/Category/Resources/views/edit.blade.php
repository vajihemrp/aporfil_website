@extends('admin.layouts.master')
@section('content')
    {{--breadcrumb--}}
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li>
                    <a href="#">خانه</a>
                </li>
                <li class="active">
                    <strong>دسته بندی</strong>
                </li>
                <li class="active">
                    <strong>ایجاد دسته بندی</strong>
                </li>

            </ol>
        </div>
    </div>

    {{--main content--}}
    <div class="wrapper wrapper-content">
        <div class="animated fadeInLeft">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins" >
                        <div class="ibox-title" style="padding-top: 10px;padding-right: 20px;border-style:none">
                            <h3 class="pull-right">ایجاد دسته بندی</h3>
                        </div>
                        <div class="ibox-content">
                            <form class="category-form" method="POST" action="{{ url('admin/categories',[$category->id]) }}"  >
                                @csrf
                                @method('PATCH')

                                <div class="form-group col-sm-6 col-xs-6 pull-right">
                                    <label class="control-label col-sm-2 col-xs-2 pull-right" for="name">عنوان دسته<sup class="super">&nbsp;&nbsp;*</sup></label>

                                    <div class="col-sm-6 col-xs-6 ">
                                        <input type="text" class="form-control" name="name" id="name" required
                                               value="{{ old('name', $category->name ) }}"
                                               oninvalid="this.setCustomValidity('لطفا این فیلد را پر کنید')"
                                               oninput="setCustomValidity('')">
                                    </div>

                                </div>

                                {{--<div class="switch col-sm-6 col-xs-6 myswitch">--}}
                                    {{--<div class="onoffswitch">--}}
                                        {{--@if($category['switch']==1)--}}
                                            {{--<input name="status" type="checkbox" value="1"  class="onoffswitch-checkbox" id="checkbox">--}}
                                        {{--@else--}}
                                            {{--<input name="status" type="checkbox" value="0"  class="onoffswitch-checkbox" id="checkbox">--}}
                                        {{--@endif--}}
                                        {{--<label class="onoffswitch-label" for="example2">--}}
                                                {{--<span class="onoffswitch-inner">--}}
                                                {{--</span>--}}
                                            {{--<span class="onoffswitch-switch">--}}
                                                {{--</span>--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="clearfix"></div>

                                <input type="submit" class="btn btn-success btn-block btn-sm submit_btn" name="submit"  value="ارسال">

                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<script>
//    function change() {
//        alert(e);
//        if(this.value()==1){
//            $('#checkbox').val('0');
//        }else{
//            $('#checkbox').val('1');
//        }
//    }
</script>
@endsection