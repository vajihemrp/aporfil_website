@extends('admin.layouts.master')
@section('content')
    {{--breadcrumb--}}
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li>
                    <a href="#">خانه</a>
                </li>
                <li class="active">
                    <strong>دسته بندی</strong>
                </li>
                <li class="active">
                    <strong>ایجاد دسته بندی</strong>
                </li>

            </ol>
        </div>
    </div>

    {{--main content--}}
    <div class="wrapper wrapper-content">
        <div class="animated fadeInLeft">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins" >
                        <div class="ibox-title" style="padding-top: 10px;padding-right: 20px;border-style:none">
                            <h3 class="pull-right">ایجاد دسته بندی</h3>
                        </div>
                        <div class="ibox-content">
                            <form class="category-form" method="POST" action="{{ url('admin/categories') }}"  >
                                {{ csrf_field() }}

                                <div class="form-group col-sm-6 col-xs-6 pull-right">
                                    <label class="control-label col-sm-2 col-xs-2 pull-right" for="name">عنوان دسته<sup class="super">&nbsp;&nbsp;*</sup></label>

                                    <div class="col-sm-6 col-xs-6 ">
                                        <input type="text" class="form-control" name="name" id="name" required
                                               oninvalid="this.setCustomValidity('لطفا این فیلد را پر کنید')"
                                               oninput="setCustomValidity('')">
                                    </div>

                                </div>

                                <div class="switch col-sm-6 col-xs-6 myswitch">
                                        <div class="onoffswitch">
                                            <input name="status" value="1" type="checkbox" class="onoffswitch-checkbox" id="example2">
                                            <label class="onoffswitch-label" for="example2">
                                                <span class="onoffswitch-inner">
                                                </span>
                                                <span class="onoffswitch-switch">
                                                </span>
                                            </label>
                                        </div>
                                    </div>

                                {{--<div class="ibox-content">--}}
                                    {{--<div class="switch">--}}
                                        {{--<div class="onoffswitch">--}}
                                            {{--<input type="checkbox" class="onoffswitch-checkbox" id="example2">--}}
                                            {{--<label class="onoffswitch-label" for="example2">--}}
                                                {{--<span class="onoffswitch-inner">--}}
                                                {{--</span>--}}
                                                {{--<span class="onoffswitch-switch">--}}
                                                {{--</span>--}}
                                            {{--</label>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="clearfix"></div>

                                <input type="submit" class="btn btn-success btn-block btn-sm submit_btn" name="submit"  value="ارسال">

                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection