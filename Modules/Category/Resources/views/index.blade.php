@extends('admin.layouts.master')
@section('content')
{{--breadcrumb--}}
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-xs-12">
        <ol class="breadcrumb">
            <li>
                <a href="#">خانه</a>
            </li>
            <li class="active">
                <strong>دسته بندی</strong>
            </li>

        </ol>
    </div>
</div>

{{--main content--}}
<div class="wrapper wrapper-content" id="right-box" >
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins" >
                <div class="ibox-title" style="padding-top: 10px;padding-right: 20px;border-style:none">
                    <h3 class="pull-right">همه دسته بندی ها</h3>
                    <a href="{{url('admin/categories/create')}}" type="button" class="btn btn-info pull-left" >افزودن دسته بندی</a>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <div class="table-responsive">
                            <table  class="table table-hover table-bordered dataTables-example" cellspacing="0" width="100%" id="category_table">
                                <thead>
                                <tr>
                                    <th>نام</th>
                                    <td>وضعیت</td>
                                    <th>عملیات</th>
                                </tr>
                                </thead>
                                <tbody id="company_result">
                                @forelse($categories as $category)
                                    <tr>
                                        <td>{{$category->name}}</td>

                                        <td style="text-align: center">
                                            @if($category->status)
                                                <a  id="status-item" type="button" class="btn btn-primary btn-sm" href="{{url('/admin/categories/changeStatus',[$category->id])}}" style="width: 100px" title="تغییر وضعیت">فعال</a>
                                            @else
                                                <a  id="status-item" type="button" class="btn btn-danger btn-sm" href="{{url('/admin/categories/changeStatus',[$category->id])}}" style="width: 100px" title="تغییر وضعیت">غیرفعال</a>

                                            @endif

                                        </td>
                                        <td>
                                            <a  id="edit-item" href="{{url('admin/categories/'.$category->id.'/edit')}}" title="ویرایش">
                                                <i class="fa fa-pencil" style="color: black;font-size: large;margin-right: 8%" ></i>
                                            </a>

                                            {{--<a  id="parcel-item" href="javascript:void()" title="افزودن دسته بندی">--}}
                                            {{--<i class="fa fa" style="color: lightgray;font-size: large;margin-right: 8%" ></i>--}}
                                            {{--</a>--}}
                                            {{--@if($category->status==1)--}}
                                            {{--<a  id="edit-item" href="{{url('/category/'.$category->id.'/edit')}}" title="ویرایش">--}}
                                            {{--<i class="fas fa-pencil-alt" style="color: black;font-size: large;margin-right: 8%" ></i>--}}
                                            {{--</a>--}}

                                            {{--<a  id="parcel-item" href="{{url('/category/create',[$category->id])}}" title="افزودن دسته بندی">--}}
                                            {{--<i class="fa fa-fw fa-box-open" style="color: black;font-size: large;margin-right: 8%" ></i>--}}
                                            {{--</a>--}}
                                            {{--@else--}}
                                            {{--<a  id="edit-item" href="javascript:void(0)" title="ویرایش">--}}
                                            {{--<i class="fas fa-pencil-alt" style="color: black;font-size: large;margin-right: 8%" ></i>--}}
                                            {{--</a>--}}

                                            {{--<a  id="parcel-item" href="javascript:void()" title="افزودن دسته بندی">--}}
                                            {{--<i class="fa fa-fw fa-box-open" style="color: lightgray;font-size: large;margin-right: 8%" ></i>--}}
                                            {{--</a>--}}

                                            {{--@endif--}}

                                            {{--@if($url=='admin')--}}
                                            {{--<a  id="parcel-item" href="{{url('admin/users/edit',[$category['id'],'position'=>'customer'])}}" title="user setting">--}}
                                            {{--<i class="fa fa-fw fa-cog" style="color: black;font-size: large" ></i>--}}
                                            {{--</a>--}}
                                            {{--@endif--}}

                                            <a href="javascript:void(0)" id="del_func" onclick="confirmDelete('delete-{{ $category['id']  }}')">
                                                <i class="fa fa-trash" style="color: black;font-size: large"></i>
                                            </a>

                                            <form action="{{ url('/admin/categories', $category->id) }}" method="post" id="delete-{{ $category->id }}" style="display: none">
                                                @csrf
                                                @method('DELETE')
                                            </form>

                                        </td>
                                    </tr>

                                @empty
                                    <tr>
                                        <td colspan="8" class="text-lg-left">اطلاعاتی یافت نشد</td>
                                    </tr>

                                @endforelse


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    function confirmDelete(formId) {
        alert('ok');
    }
</script>
@endsection