<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::prefix('category')->group(function() {
//    Route::get('/', 'CategoryController@index');
//});

Route::prefix('admin')->group(function() {
    Route::resource('/categories', 'CategoriesController');
});

Route::prefix('website')->group(function() {
    Route::get('/categories', 'CategoryController@index');
//    Route::get('/categories/{category}', 'CategoryController@show');
    Route::get('/category/{slug}', 'CategoryController@categorizable_type');
});
