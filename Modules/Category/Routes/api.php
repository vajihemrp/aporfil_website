<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/category', function (Request $request) {
//    return $request->user();
//});

Route::middleware('auth:api-admin')->prefix('/admin')->group(function() {
    Route::resource('/categories', 'CategoriesController');
    Route::get('/list/categories', 'CategoriesController@list');
    // Route::get('/changeStatus/categories/{id}', 'CategoriesController@changeStatus');
});

Route::middleware([])->prefix('/website')->group(function() {
    Route::get('/categories', 'CategoryController@index');
    Route::get('/categories/{category}', 'CategoryController@show');
});
