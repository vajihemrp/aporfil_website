<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{locale}', function (string $locale) {

    if (!in_array($locale,['en','fa'])) {
        abort(400);
    }

    App::setLocale($locale);
    return view('welcome',compact('locale'));
});

Route::get('/', function () {
    $locale = 'fa';
    return view('welcome',compact('locale'));
});
