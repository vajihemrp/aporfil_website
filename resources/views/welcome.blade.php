<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <title>Aporfil</title>
    <link rel="icon" type="image/png" href="{{asset('images/main-logo/logo.png')}}">
    <!-- fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i">
    <!-- css -->
    <link rel="stylesheet" href="{{asset('vendor/bootstrap-4.2.1/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/owl-carousel-2.3.4/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ $locale == 'fa' ? asset('css/style.rtl.css') : asset('css/style.css')}}">
    <!-- js -->
    <script src="{{asset('vendor/jquery-3.3.1/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap-4.2.1/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('vendor/owl-carousel-2.3.4/owl.carousel.min.js')}}"></script>
    <script src="{{asset('vendor/nouislider-12.1.0/nouislider.min.js')}}"></script>
    <script src="{{asset('js/number.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('vendor/svg4everybody-2.1.9/svg4everybody.min.js')}}"></script>
    <script>svg4everybody();</script>
    <!-- font - fontawesome -->
    <link rel="stylesheet" href="{{asset('vendor/fontawesome-5.6.1/css/all.min.css')}}">
    <!-- font  -->
    <link rel="stylesheet" href="{{asset('fonts/stroyka/stroyka.css')}}">
</head>

<body>
<!-- quickview-modal -->
{{--<div id="quickview-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">--}}
{{--    <div class="modal-dialog modal-dialog-centered modal-xl">--}}
{{--        <div class="modal-content"></div>--}}
{{--    </div>--}}
{{--</div>--}}
<!-- quickview-modal / end -->
<!-- mobilemenu -->
<div class="mobilemenu">
    <div class="mobilemenu__backdrop"></div>
    <div class="mobilemenu__body">
        <div class="mobilemenu__header">
            <div class="mobilemenu__title">{{__('messages.menu')}}</div>
            <button type="button" class="mobilemenu__close">
                <svg width="20px" height="20px">
                    <use xlink:href="images/sprite.svg#cross-20"></use>
                </svg>
            </button>
        </div>
        <div class="mobilemenu__content">
            <ul class="mobile-links mobile-links--level--0" data-collapse data-collapse-opened-class="mobile-links__item--open">
                <li class="mobile-links__item" data-collapse-item>
                    <div class="mobile-links__item-title">
                        <a href="/{{$locale}}" class="mobile-links__item-link">خانه</a>
                    </div>
                </li>
                <li class="mobile-links__item" data-collapse-item>
                    <div class="mobile-links__item-title">
                        <a href="" class="mobile-links__item-link">{{__('messages.products')}}</a>
                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                            <svg class="mobile-links__item-arrow" width="12px" height="7px">
                                <use xlink:href="images/sprite.svg#arrow-rounded-down-12x7"></use>
                            </svg>
                        </button>
                    </div>
                    <div class="mobile-links__item-sub-links" data-collapse-content>
                        <ul class="mobile-links mobile-links--level--1">
                            <li class="mobile-links__item" data-collapse-item>
                                <div class="mobile-links__item-title">
                                    <a href="" class="mobile-links__item-link">Power Tools</a>
                                    <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                                        <svg class="mobile-links__item-arrow" width="12px" height="7px">
                                            <use xlink:href="images/sprite.svg#arrow-rounded-down-12x7"></use>
                                        </svg>
                                    </button>
                                </div>
                                <div class="mobile-links__item-sub-links" data-collapse-content>
                                    <ul class="mobile-links mobile-links--level--2">
                                        <li class="mobile-links__item" data-collapse-item>
                                            <div class="mobile-links__item-title">
                                                <a href="" class="mobile-links__item-link">Engravers</a>
                                            </div>
                                        </li>
                                        <li class="mobile-links__item" data-collapse-item>
                                            <div class="mobile-links__item-title">
                                                <a href="" class="mobile-links__item-link">Wrenches</a>
                                            </div>
                                        </li>
                                        <li class="mobile-links__item" data-collapse-item>
                                            <div class="mobile-links__item-title">
                                                <a href="" class="mobile-links__item-link">Wall Chaser</a>
                                            </div>
                                        </li>
                                        <li class="mobile-links__item" data-collapse-item>
                                            <div class="mobile-links__item-title">
                                                <a href="" class="mobile-links__item-link">Pneumatic Tools</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="mobile-links__item" data-collapse-item>
                                <div class="mobile-links__item-title">
                                    <a href="" class="mobile-links__item-link">Machine Tools</a>
                                    <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                                        <svg class="mobile-links__item-arrow" width="12px" height="7px">
                                            <use xlink:href="images/sprite.svg#arrow-rounded-down-12x7"></use>
                                        </svg>
                                    </button>
                                </div>
                                <div class="mobile-links__item-sub-links" data-collapse-content>
                                    <ul class="mobile-links mobile-links--level--2">
                                        <li class="mobile-links__item" data-collapse-item>
                                            <div class="mobile-links__item-title">
                                                <a href="" class="mobile-links__item-link">Thread Cutting</a>
                                            </div>
                                        </li>
                                        <li class="mobile-links__item" data-collapse-item>
                                            <div class="mobile-links__item-title">
                                                <a href="" class="mobile-links__item-link">Chip Blowers</a>
                                            </div>
                                        </li>
                                        <li class="mobile-links__item" data-collapse-item>
                                            <div class="mobile-links__item-title">
                                                <a href="" class="mobile-links__item-link">Sharpening Machines</a>
                                            </div>
                                        </li>
                                        <li class="mobile-links__item" data-collapse-item>
                                            <div class="mobile-links__item-title">
                                                <a href="" class="mobile-links__item-link">Pipe Cutters</a>
                                            </div>
                                        </li>
                                        <li class="mobile-links__item" data-collapse-item>
                                            <div class="mobile-links__item-title">
                                                <a href="" class="mobile-links__item-link">Slotting machines</a>
                                            </div>
                                        </li>
                                        <li class="mobile-links__item" data-collapse-item>
                                            <div class="mobile-links__item-title">
                                                <a href="" class="mobile-links__item-link">Lathes</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="mobile-links__item" data-collapse-item>
                    <div class="mobile-links__item-title">
                        <a href="/" class="mobile-links__item-link">اخبار</a>
                    </div>
                </li>
                <li class="mobile-links__item" data-collapse-item>
                    <div class="mobile-links__item-title">
                        <a href="/" class="mobile-links__item-link">درباره ما</a>
                    </div>
                </li>
                <li class="mobile-links__item" data-collapse-item>
                    <div class="mobile-links__item-title">
                        <a href="/" class="mobile-links__item-link">تماس با ما</a>
                    </div>
                </li>
{{--                <li class="mobile-links__item" data-collapse-item>--}}
{{--                    <div class="mobile-links__item-title">--}}
{{--                        <a data-collapse-trigger class="mobile-links__item-link">Currency</a>--}}
{{--                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>--}}
{{--                            <svg class="mobile-links__item-arrow" width="12px" height="7px">--}}
{{--                                <use xlink:href="images/sprite.svg#arrow-rounded-down-12x7"></use>--}}
{{--                            </svg>--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                    <div class="mobile-links__item-sub-links" data-collapse-content>--}}
{{--                        <ul class="mobile-links mobile-links--level--1">--}}
{{--                            <li class="mobile-links__item" data-collapse-item>--}}
{{--                                <div class="mobile-links__item-title">--}}
{{--                                    <a href="" class="mobile-links__item-link">t iran tooman</a>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="mobile-links__item" data-collapse-item>--}}
{{--                                <div class="mobile-links__item-title">--}}
{{--                                    <a href="" class="mobile-links__item-link">$ US Dollar</a>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
                <li class="mobile-links__item" data-collapse-item>
                    <div class="mobile-links__item-title">
                        <a data-collapse-trigger class="mobile-links__item-link">{{__('messages.language')}}</a>
                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                            <svg class="mobile-links__item-arrow" width="12px" height="7px">
                                <use xlink:href="images/sprite.svg#arrow-rounded-down-12x7"></use>
                            </svg>
                        </button>
                    </div>
                    <div class="mobile-links__item-sub-links" data-collapse-content>
                        <ul class="mobile-links mobile-links--level--1">
                            <li class="mobile-links__item" data-collapse-item>
                                <div class="mobile-links__item-title">
                                    <a href="" class="mobile-links__item-link">En</a>
                                </div>
                            </li>
                            <li class="mobile-links__item" data-collapse-item>
                                <div class="mobile-links__item-title">
                                    <a href="" class="mobile-links__item-link">Fa</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- mobilemenu / end -->
<!-- site -->
<div class="site">
    <!-- mobile site__header -->
    <header class="site__header d-lg-none">
        <div class="mobile-header mobile-header--sticky mobile-header--stuck">
            <div class="mobile-header__panel">
                <div class="container">
                    <div class="mobile-header__body">
                        <button class="mobile-header__menu-button">
                            <svg width="18px" height="14px">
                                <use xlink:href="images/sprite.svg#menu-18x14"></use>
                            </svg>
                        </button>
                        <a class="mobile-header__logo" href="index.html">
                            <img src="{{asset('images/main-logo/logo.png')}}" height="50px">
                        </a>
                        <div class="mobile-header__search">
                            <form class="mobile-header__search-form" action="">
                                <input class="mobile-header__search-input" name="search" placeholder="{{__('messages.searchMsg')}}" aria-label="Site search" type="text" autocomplete="off">
                                <button class="mobile-header__search-button mobile-header__search-button--submit" type="submit">
                                    <svg width="20px" height="20px">
                                        <use xlink:href="images/sprite.svg#search-20"></use>
                                    </svg>
                                </button>
                                <button class="mobile-header__search-button mobile-header__search-button--close" type="button">
                                    <svg width="20px" height="20px">
                                        <use xlink:href="images/sprite.svg#cross-20"></use>
                                    </svg>
                                </button>
                                <div class="mobile-header__search-body"></div>
                            </form>
                        </div>
                        <div class="mobile-header__indicators">
                            <div class="indicator indicator--mobile-search indicator--mobile d-sm-none">
                                <button class="indicator__button">
                                        <span class="indicator__area">
                                            <svg width="20px" height="20px">
                                                <use xlink:href="images/sprite.svg#search-20"></use>
                                            </svg>
                                        </span>
                                </button>
                            </div>
{{--                            <div class="indicator indicator--mobile d-sm-flex d-none">--}}
{{--                                <a href="wishlist.html" class="indicator__button">--}}
{{--                                        <span class="indicator__area">--}}
{{--                                            <svg width="20px" height="20px">--}}
{{--                                                <use xlink:href="images/sprite.svg#heart-20"></use>--}}
{{--                                            </svg>--}}
{{--                                            <span class="indicator__value">0</span>--}}
{{--                                        </span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="indicator indicator--mobile">--}}
{{--                                <a href="cart.html" class="indicator__button">--}}
{{--                                        <span class="indicator__area">--}}
{{--                                            <svg width="20px" height="20px">--}}
{{--                                                <use xlink:href="images/sprite.svg#cart-20"></use>--}}
{{--                                            </svg>--}}
{{--                                            <span class="indicator__value">3</span>--}}
{{--                                        </span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- mobile site__header / end -->
    <!-- desktop site__header -->
    <header class="site__header d-lg-block d-none">
        <div class="site-header">
            <!-- .topbar -->
            <div class="site-header__topbar topbar">
                <div class="topbar__container container">
                    <div class="topbar__row">
{{--                        <div class="topbar__item topbar__item--link">--}}
{{--                            <a class="topbar-link" href="about-us.html">About Us</a>--}}
{{--                        </div>--}}
{{--                        <div class="topbar__item topbar__item--link">--}}
{{--                            <a class="topbar-link" href="contact-us.html">Contacts</a>--}}
{{--                        </div>--}}
{{--                        <div class="topbar__item topbar__item--link">--}}
{{--                            <a class="topbar-link" href="">Store Location</a>--}}
{{--                        </div>--}}
{{--                        <div class="topbar__item topbar__item--link">--}}
{{--                            <a class="topbar-link" href="track-order.html">Track Order</a>--}}
{{--                        </div>--}}
{{--                        <div class="topbar__item topbar__item--link">--}}
{{--                            <a class="topbar-link" href="blog-classic.html">Blog</a>--}}
{{--                        </div>--}}
{{--                        <div class="topbar__spring"></div>--}}
{{--                        <div class="topbar__item">--}}
{{--                            <div class="topbar-dropdown">--}}
{{--                                <button class="topbar-dropdown__btn" type="button">--}}
{{--                                    My Account--}}
{{--                                    <svg width="7px" height="5px">--}}
{{--                                        <use xlink:href="images/sprite.svg#arrow-rounded-down-7x5"></use>--}}
{{--                                    </svg>--}}
{{--                                </button>--}}
{{--                                <div class="topbar-dropdown__body">--}}
{{--                                    <!-- .menu -->--}}
{{--                                    <ul class="menu menu--layout--topbar ">--}}
{{--                                        <li>--}}
{{--                                            <a href="account.html">--}}
{{--                                                Login--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="account.html">--}}
{{--                                                Register--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="">--}}
{{--                                                Orders--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="">--}}
{{--                                                Addresses--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                    <!-- .menu / end -->--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="topbar__item">--}}
{{--                            <div class="topbar-dropdown">--}}
{{--                                <button class="topbar-dropdown__btn" type="button">--}}
{{--                                    {{__('messages.currency')}}: <span class="topbar__item-value">USD</span>--}}
{{--                                    <svg width="7px" height="5px">--}}
{{--                                        <use xlink:href="images/sprite.svg#arrow-rounded-down-7x5"></use>--}}
{{--                                    </svg>--}}
{{--                                </button>--}}
{{--                                <div class="topbar-dropdown__body">--}}
{{--                                    <!-- .menu -->--}}
{{--                                    <ul class="menu menu--layout--topbar ">--}}
{{--                                        <li>--}}
{{--                                            <a href="">--}}
{{--                                                Toman t--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="">--}}
{{--                                                $ US Dollar--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                    <!-- .menu / end -->--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="topbar__item">
                            <div class="topbar-dropdown">
                                <button class="topbar-dropdown__btn" type="button">
{{--                                    {{__('messages.language')}}: --}}
                                    <span class="topbar__item-value">EN</span>
                                    <svg width="7px" height="5px">
                                        <use xlink:href="images/sprite.svg#arrow-rounded-down-7x5"></use>
                                    </svg>
                                </button>
                                <div class="topbar-dropdown__body">
                                    <!-- .menu -->
                                    <ul class="menu menu--layout--topbar  menu--with-icons ">
                                        <li>
                                            <a href="">
{{--                                                <div class="menu__icon"><img srcset="images/languages/language-1.png 1x, images/languages/language-1@2x.png 2x" src="images/languages/language-1.png" alt=""></div>--}}
                                                En
                                            </a>
                                        </li>
                                        <li>
                                            <a href="">
{{--                                                <div class="menu__icon"><img srcset="images/languages/language-2.png 1x, images/languages/language-2@2x.png 2x" src="images/languages/language-2.png" alt=""></div>--}}
                                                Fa
                                            </a>
                                        </li>

                                    </ul>
                                    <!-- .menu / end -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .topbar / end -->
            <div class="site-header__nav-panel">
                <div class="nav-panel ">
                    <div class="nav-panel__container container">
                        <div class="nav-panel__row">
                            <div class="nav-panel__logo">
                                <a href="index.html">
                                    <img src="{{asset('images/main-logo/logo.png')}}" height="50px">
                                </a>
                            </div>
                            <!-- .nav-links -->
                            <div class="nav-panel__nav-links nav-links">
                                <ul class="nav-links__list">
                                    <li class="nav-links__item ">
                                        <a href="/{{$locale}}">
                                                <span>
                                                    خانه
                                                </span>
                                        </a>
                                    </li>
                                    <li class="nav-links__item  nav-links__item--with-submenu ">
                                        <a href="">
                                                <span>
                                                   محصولات
                                                    <svg class="nav-links__arrow" width="9px" height="6px">
                                                        <use xlink:href="images/sprite.svg#arrow-rounded-down-9x6"></use>
                                                    </svg>
                                                </span>
                                        </a>
                                        <div class="nav-links__megamenu nav-links__megamenu--size--nl">
                                            <!-- .megamenu -->
                                            <div class="megamenu ">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <ul class="megamenu__links megamenu__links--level--0">
                                                            <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                <a href="">{{__('messages.profile')}}</a>
                                                                <ul class="megamenu__links megamenu__links--level--1">
                                                                    <li class="megamenu__item"><a href="">{{__('messages.squareBoxProfile')}}</a></li>
                                                                    <li class="megamenu__item"><a href="">{{__('messages.rectangularBoxProfile')}}</a></li>
                                                                    <li class="megamenu__item"><a href="">{{__('messages.windowProfile')}}</a></li>
                                                                    <li class="megamenu__item"><a href="">{{__('messages.zProfile')}}</a></li>
                                                                </ul>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                    <div class="col-6">
                                                        <ul class="megamenu__links megamenu__links--level--0">
                                                            <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                <a href="">{{__('messages.pipe')}}</a>
                                                                <ul class="megamenu__links megamenu__links--level--1">
                                                                    <li class="megamenu__item"><a href="">{{__('messages.industPipe')}}</a></li>
                                                                    <li class="megamenu__item"><a href="">{{__('messages.gasPipe')}}</a></li>

                                                                </ul>

                                                            </li>
                                                            <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                <a href="">{{__('messages.sheet')}}</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- .megamenu / end -->
                                        </div>
                                    </li>
                                    <li class="nav-links__item ">
                                        <a href="contact-us.html">
                                                <span>
                                                    اخبار
                                                </span>
                                        </a>
                                    </li>
                                    <li class="nav-links__item ">
                                        <a href="contact-us.html">
                                                <span>
                                                    همکاری ما
                                                </span>
                                        </a>
                                    </li>
                                    <li class="nav-links__item ">
                                        <a href="contact-us.html">
                                                <span>
                                                    درباره ما
                                                </span>
                                        </a>
                                    </li>
                                    <li class="nav-links__item ">
                                        <a href="contact-us.html">
                                                <span>
                                                    تماس با ما
                                                </span>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                            <!-- .nav-links / end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- desktop site__header / end -->
    <!-- site__body -->
    <div class="site__body">
        <!-- .block-slideshow -->
        <div class="block-slideshow block-slideshow--layout--full block">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="block-slideshow__body">
                            <div class="owl-carousel">
                                <a class="block-slideshow__slide" href="">
                                    <div class="block-slideshow__slide-image block-slideshow__slide-image--desktop" style="background-image: url('images/slides/slide-1-full.jpg')"></div>
                                    <div class="block-slideshow__slide-image block-slideshow__slide-image--mobile" style="background-image: url('images/slides/slide-1-mobile.jpg')"></div>
                                    <div class="block-slideshow__slide-content">
                                        <div class="block-slideshow__slide-title">Big choice of<br>Plumbing products</div>
                                        <div class="block-slideshow__slide-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Etiam pharetra laoreet dui quis molestie.</div>
                                        <div class="block-slideshow__slide-button">
                                            <span class="btn btn-primary btn-lg">Shop Now</span>
                                        </div>
                                    </div>
                                </a>
                                <a class="block-slideshow__slide" href="">
                                    <div class="block-slideshow__slide-image block-slideshow__slide-image--desktop" style="background-image: url('images/slides/slide-2-full.jpg')"></div>
                                    <div class="block-slideshow__slide-image block-slideshow__slide-image--mobile" style="background-image: url('images/slides/slide-2-mobile.jpg')"></div>
                                    <div class="block-slideshow__slide-content">
                                        <div class="block-slideshow__slide-title">Screwdrivers<br>Professional Tools</div>
                                        <div class="block-slideshow__slide-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Etiam pharetra laoreet dui quis molestie.</div>
                                        <div class="block-slideshow__slide-button">
                                            <span class="btn btn-primary btn-lg">Shop Now</span>
                                        </div>
                                    </div>
                                </a>
                                <a class="block-slideshow__slide" href="">
                                    <div class="block-slideshow__slide-image block-slideshow__slide-image--desktop" style="background-image: url('images/slides/slide-3-full.jpg')"></div>
                                    <div class="block-slideshow__slide-image block-slideshow__slide-image--mobile" style="background-image: url('images/slides/slide-3-mobile.jpg')"></div>
                                    <div class="block-slideshow__slide-content">
                                        <div class="block-slideshow__slide-title">One more<br>Unique header</div>
                                        <div class="block-slideshow__slide-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Etiam pharetra laoreet dui quis molestie.</div>
                                        <div class="block-slideshow__slide-button">
                                            <span class="btn btn-primary btn-lg">Shop Now</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .block-slideshow / end -->
        <!-- block-about-us -->
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="block">
                        <div class="posts-view">
                            <div class="posts-view__list posts-list posts-list--layout--list">
                                <div class="posts-list__body">
                                    <div class="posts-list__item">
                                        <div class="post-card post-card--layout--list post-card--size--nl">
                                            <div class="post-card__image">
                                                <a href="">
                                                    <img src="images/posts/post-1.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="post-card__info">
                                                <div class="post-card__category">
{{--                                                    <a href="">Special Offers</a>--}}
                                                    {{__('messages.aboutUs')}}
                                                </div>

                                                <div class="post-card__content">
                                                   {{__('messages.aboutShortMsg')}}
                                                </div>
                                                <div class="post-card__read-more">
                                                    <a href="" class="btn btn-secondary btn-sm">{{__('messages.readMore')}}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- lock-about-us -->
        <!-- .block-products -->
        <div class="block block-products block-products--layout--large-last">
            <div class="container">
                <div class="block-header">
                    <h3 class="block-header__title">{{ __('messages.products') }}</h3>
                    <div class="block-header__divider"></div>
                </div>
                <div class="block-products__body">
                    <div class="block-products__list">
                        <div class="block-products__list-item">
                            <div class="product-card ">
{{--                                <button class="product-card__quickview" type="button">--}}
{{--                                    <svg width="16px" height="16px">--}}
{{--                                        <use xlink:href="images/sprite.svg#quickview-16"></use>--}}
{{--                                    </svg>--}}
{{--                                    <span class="fake-svg-icon"></span>--}}
{{--                                </button>--}}
{{--                                <div class="product-card__badges-list">--}}
{{--                                    <div class="product-card__badge product-card__badge--hot">پرفروش</div>--}}
{{--                                </div>--}}
                                <div class="product-card__image">
                                    <a href="product.html"><img src="images/products/product-2.jpg" alt=""></a>
                                </div>
                                <div class="product-card__info">
                                    <div class="product-card__name">
                                        <a href="product.html">{{__('messages.squareBoxProfile')}}</a>
                                    </div>
                                    <ul class="product-card__features-list" style="display: block">
                                        <li><span>{{__('messages.size')}}</span>:&nbsp;&nbsp;<span dir="ltr">15*15mm - 100*100 mm</span></li>
                                        <li><span>{{__('messages.thickness')}}</span>:&nbsp;&nbsp;<span dir="ltr">1.2 mm - 4mm</span></li>
                                    </ul>
                                </div>
                                <div class="product-card__actions">
{{--                                    <div class="product-card__availability">--}}
{{--                                        Availability: <span class="text-success">In Stock</span>--}}
{{--                                    </div>--}}
                                    <div class="product-card__prices">
{{--                                        $1,019.00--}}
                                    </div>
                                    <div class="product-card__buttons">
                                        <button class="btn btn-primary product-card__addtocart" type="button">{{__('messages.readMore')}}</button>
{{--                                        <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>--}}

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products__list-item">
                            <div class="product-card ">
{{--                                <button class="product-card__quickview" type="button">--}}
{{--                                    <svg width="16px" height="16px">--}}
{{--                                        <use xlink:href="images/sprite.svg#quickview-16"></use>--}}
{{--                                    </svg>--}}
{{--                                    <span class="fake-svg-icon"></span>--}}
{{--                                </button>--}}
                                <div class="product-card__image">
                                    <a href="product.html"><img src="images/products/product-3.jpg" alt=""></a>
                                </div>
                                <div class="product-card__info">
                                    <div class="product-card__name">
                                        <a href="product.html">{{__('messages.rectangularBoxProfile')}}</a>
                                    </div>

                                    <ul class="product-card__features-list" style="display: block">
                                        <li><span>{{__('messages.size')}}</span>:&nbsp;&nbsp;<span dir="ltr">10*20mm - 100*180 mm</span></li>
                                        <li><span>{{__('messages.thickness')}}</span>:&nbsp;&nbsp;<span dir="ltr">1.2 mm - 4mm</span></li>
                                    </ul>
                                </div>
                                <div class="product-card__actions">
{{--                                    <div class="product-card__availability">--}}
{{--                                        Availability: <span class="text-success">In Stock</span>--}}
{{--                                    </div>--}}
                                    <div class="product-card__prices">
{{--                                        $850.00--}}
                                    </div>
                                    <div class="product-card__buttons">
                                        <button class="btn btn-primary product-card__addtocart" type="button"> {{__('messages.readMore')}} </button>
{{--                                        <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products__list-item">
                            <div class="product-card ">
{{--                                <button class="product-card__quickview" type="button">--}}
{{--                                    <svg width="16px" height="16px">--}}
{{--                                        <use xlink:href="images/sprite.svg#quickview-16"></use>--}}
{{--                                    </svg>--}}
{{--                                    <span class="fake-svg-icon"></span>--}}
{{--                                </button>--}}
{{--                                <div class="product-card__badges-list">--}}
{{--                                    <div class="product-card__badge product-card__badge--sale">Sale</div>--}}
{{--                                </div>--}}
                                <div class="product-card__image">
                                    <a href="product.html"><img src="images/products/product-4.jpg" alt=""></a>
                                </div>
                                <div class="product-card__info">
                                    <div class="product-card__name">
                                        <a href="product.html">{{__('messages.windowProfile')}}</a>
                                    </div>

                                    <ul class="product-card__features-list" style="display: block">
                                        <li><span>{{__('messages.size')}}</span>:&nbsp;&nbsp;<span dir="ltr">10*20mm - 100*180 mm</span></li>
                                        <li><span>{{__('messages.thickness')}}</span>:&nbsp;&nbsp;<span dir="ltr">1.2 mm - 4mm</span></li>
                                    </ul>
                                </div>
                                <div class="product-card__actions">
                                    <div class="product-card__availability">
                                        Availability: <span class="text-success">In Stock</span>
                                    </div>
                                    <div class="product-card__prices">
{{--                                        <span class="product-card__new-price">$949.00</span>--}}
{{--                                        <span class="product-card__old-price">$1189.00</span>--}}
                                    </div>
                                    <div class="product-card__buttons">
                                        <button class="btn btn-primary product-card__addtocart" type="button"> {{__('messages.readMore')}} </button>
{{--                                        <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>--}}
{{--                                        <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products__list-item">
                            <div class="product-card ">
{{--                                <button class="product-card__quickview" type="button">--}}
{{--                                    <svg width="16px" height="16px">--}}
{{--                                        <use xlink:href="images/sprite.svg#quickview-16"></use>--}}
{{--                                    </svg>--}}
{{--                                    <span class="fake-svg-icon"></span>--}}
{{--                                </button>--}}
                                <div class="product-card__image">
                                    <a href="product.html"><img src="images/products/product-5.jpg" alt=""></a>
                                </div>
                                <div class="product-card__info">
                                    <div class="product-card__name">
                                        <a href="product.html">{{__('messages.zProfile')}}</a>
                                    </div>

                                    <ul class="product-card__features-list" style="display: block">
                                        <li><span>{{__('messages.size')}}</span>:&nbsp;&nbsp;<span dir="ltr">10*20mm - 100*180 mm</span></li>
                                        <li><span>{{__('messages.thickness')}}</span>:&nbsp;&nbsp;<span dir="ltr">1.2 mm - 4mm</span></li>
                                    </ul>
                                </div>
                                <div class="product-card__actions">
{{--                                    <div class="product-card__availability">--}}
{{--                                        Availability: <span class="text-success">In Stock</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="product-card__prices">--}}
{{--                                        $1,700.00--}}
{{--                                    </div>--}}
                                    <div class="product-card__buttons">
                                        <button class="btn btn-primary product-card__addtocart" type="button"> {{__('messages.readMore')}} </button>
{{--                                        <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>--}}
{{--                                        <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products__list-item">
                            <div class="product-card ">
{{--                                <button class="product-card__quickview" type="button">--}}
{{--                                    <svg width="16px" height="16px">--}}
{{--                                        <use xlink:href="images/sprite.svg#quickview-16"></use>--}}
{{--                                    </svg>--}}
{{--                                    <span class="fake-svg-icon"></span>--}}
{{--                                </button>--}}
                                <div class="product-card__image">
                                    <a href="product.html"><img src="images/products/product-6.jpg" alt=""></a>
                                </div>
                                <div class="product-card__info">
                                    <div class="product-card__name">
                                        <a href="product.html">{{__('message.pipe')}}</a>
                                    </div>

                                    <ul class="product-card__features-list" style="display: block">
                                        <li><span>{{__('messages.size')}}</span>:&nbsp;&nbsp;<span dir="ltr">10*20mm - 100*180 mm</span></li>
                                        <li><span>{{__('messages.thickness')}}</span>:&nbsp;&nbsp;<span dir="ltr">1.2 mm - 4mm</span></li>
                                    </ul>
                                </div>
                                <div class="product-card__actions">
{{--                                    <div class="product-card__availability">--}}
{{--                                        Availability: <span class="text-success">In Stock</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="product-card__prices">--}}
{{--                                        $3,199.00--}}
{{--                                    </div>--}}
                                    <div class="product-card__buttons">
                                        <button class="btn btn-primary product-card__addtocart" type="button"> {{__('messages.readMore')}} </button>
{{--                                        <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>--}}
{{--                                        <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products__list-item">
                            <div class="product-card ">
{{--                                <button class="product-card__quickview" type="button">--}}
{{--                                    <svg width="16px" height="16px">--}}
{{--                                        <use xlink:href="images/sprite.svg#quickview-16"></use>--}}
{{--                                    </svg>--}}
{{--                                    <span class="fake-svg-icon"></span>--}}
{{--                                </button>--}}
                                <div class="product-card__image">
                                    <a href="product.html"><img src="images/products/product-7.jpg" alt=""></a>
                                </div>
                                <div class="product-card__info">
                                    <div class="product-card__name">
                                        <a href="product.html">{{__('messages.sheet')}}</a>
                                    </div>
                                    <div class="product-card__features-list" style="display: block">
                                        <p>ورق ها در اندازه ها و ابعاد مختلف و با ضخامت های مختلف تولید میشوند. </p>
                                    </div>
                                </div>
                                <div class="product-card__actions">
{{--                                    <div class="product-card__availability">--}}
{{--                                        Availability: <span class="text-success">In Stock</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="product-card__prices">--}}
{{--                                        $24.00--}}
{{--                                    </div>--}}
                                    <div class="product-card__buttons">
                                        <button class="btn btn-primary product-card__addtocart" type="button"> {{__('messages.readMore')}} </button>
{{--                                        <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>--}}
{{--                                        <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-products__featured">
                        <div class="block-products__featured-item">
                            <div class="product-card ">
{{--                                <button class="product-card__quickview" type="button">--}}
{{--                                    <svg width="16px" height="16px">--}}
{{--                                        <use xlink:href="images/sprite.svg#quickview-16"></use>--}}
{{--                                    </svg>--}}
{{--                                    <span class="fake-svg-icon"></span>--}}
{{--                                </button>--}}
{{--                                <div class="product-card__badges-list">--}}
{{--                                    <div class="product-card__badge product-card__badge--new">New</div>--}}
{{--                                </div>--}}
                                <div class="product-card__image">
                                    <a href="product.html"><img src="images/products/product-1.jpg" alt=""></a>
                                </div>
                                <div class="product-card__info">
                                    <div class="product-card__name">
                                        <a href="product.html">{{__('messages.gasPipe')}}</a>
                                    </div>
                                    <ul class="product-card__features-list" style="display: block">
                                        <li><span>{{__('messages.size')}}</span>:&nbsp;&nbsp;<span dir="ltr">21*114mm</span></li>
                                        <li><span>{{__('messages.thickness')}}</span>:&nbsp;&nbsp;<span dir="ltr">1.2 mm - 4mm</span></li>
                                    </ul>
                                    <div class="product-card__features-list" style="display: block">
                                        <p>این لوله ها با ورق فولاد مبارکه تولید میشوند و دارای استاندارد ملی ایران بوده و از بهترین کیفیت برخوردار هستند.  </p>
                                    </div>
                                </div>
                                <div class="product-card__actions">
{{--                                    <div class="product-card__availability">--}}
{{--                                        Availability: <span class="text-success">In Stock</span>--}}
{{--                                    </div>--}}
                                    <div class="product-card__prices">
{{--                                        $749.00--}}
                                    </div>
                                    <div class="product-card__buttons">
                                        <button class="btn btn-primary product-card__addtocart" type="button"> {{__('messages.readMore')}} </button>
{{--                                        <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>--}}
{{--                                        <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .block-products / end -->
        <!-- .block-banner -->
        <div class="block block-banner">
            <div class="container">
                <a href="" class="block-banner__body">
                    <div class="block-banner__image block-banner__image--desktop" style="background-image: url('images/banners/banner-1.jpg')"></div>
                    <div class="block-banner__image block-banner__image--mobile" style="background-image: url('images/banners/banner-1-mobile.jpg')"></div>
                    <div class="block-banner__title">Hundreds <br class="block-banner__mobile-br"> Hand Tools</div>
                    <div class="block-banner__text">Hammers, Chisels, Universal Pliers, Nippers, Jigsaws, Saws</div>
                    <div class="block-banner__button">
                        <span class="btn btn-sm btn-primary">Shop Now</span>
                    </div>
                </a>
            </div>
        </div>
        <!-- .block-banner / end -->

        <!-- .block-categories -->
        <div class="block block--highlighted block-categories block-categories--layout--compact">
            <div class="container">
                <div class="block-header">
                    <h3 class="block-header__title">{{__('messages.certificates')}}</h3>
                    <div class="block-header__divider"></div>
                </div>
                <div class="block-categories__list">
                    <div class="block-categories__item category-card category-card--layout--compact">
                        <div class="category-card__body">
                            <div class="category-card__image">
                                <a href=""><img src="images/categories/category-1.jpg" alt=""></a>
                            </div>
                            <div class="category-card__content">
                                <div class="category-card__name">
                                    <a href="">{{__('messages.certificateTitle1')}}</a>
                                </div>
{{--                                <ul class="category-card__links">--}}
{{--                                    <li><a href="">Screwdrivers</a></li>--}}
{{--                                    <li><a href="">Milling Cutters</a></li>--}}
{{--                                    <li><a href="">Sanding Machines</a></li>--}}
{{--                                    <li><a href="">Wrenches</a></li>--}}
{{--                                    <li><a href="">Drills</a></li>--}}
{{--                                </ul>--}}
{{--                                <div class="category-card__all">--}}
{{--                                    <a href="">Show All</a>--}}
{{--                                </div>--}}
                                <div class="category-card__products">
                                    {{__('messages.certificateDesc1')}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-categories__item category-card category-card--layout--compact">
                        <div class="category-card__body">
                            <div class="category-card__image">
                                <a href=""><img src="images/categories/category-2.jpg" alt=""></a>
                            </div>
                            <div class="category-card__content">
                                <div class="category-card__name">
                                    <a href="">{{__('messages.certificateTitle2')}}</a>
                                </div>
{{--                                <ul class="category-card__links">--}}
{{--                                    <li><a href="">Screwdrivers</a></li>--}}
{{--                                    <li><a href="">Hammers</a></li>--}}
{{--                                    <li><a href="">Spanners</a></li>--}}
{{--                                    <li><a href="">Handsaws</a></li>--}}
{{--                                    <li><a href="">Paint Tools</a></li>--}}
{{--                                </ul>--}}
{{--                                <div class="category-card__all">--}}
{{--                                    <a href="">Show All</a>--}}
{{--                                </div>--}}
                                <div class="category-card__products">
                                    {{__('messages.certificateDesc2')}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-categories__item category-card category-card--layout--compact">
                        <div class="category-card__body">
                            <div class="category-card__image">
                                <a href=""><img src="images/categories/category-4.jpg" alt=""></a>
                            </div>
                            <div class="category-card__content">
                                <div class="category-card__name">
                                    <a href="">{{__('messages.certificateTitle4')}}</a>
                                </div>
{{--                                <ul class="category-card__links">--}}
{{--                                    <li><a href="">Lathes</a></li>--}}
{{--                                    <li><a href="">Milling Machines</a></li>--}}
{{--                                    <li><a href="">Grinding Machines</a></li>--}}
{{--                                    <li><a href="">CNC Machines</a></li>--}}
{{--                                    <li><a href="">Sharpening Machines</a></li>--}}
{{--                                </ul>--}}
{{--                                <div class="category-card__all">--}}
{{--                                    <a href="">Show All</a>--}}
{{--                                </div>--}}
                                <div class="category-card__products">
                                    {{__('messages.certificateDesc4')}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-categories__item category-card category-card--layout--compact">
                        <div class="category-card__body">
                            <div class="category-card__image">
                                <a href=""><img src="images/categories/category-3.jpg" alt=""></a>
                            </div>
                            <div class="category-card__content">
                                <div class="category-card__name">
                                    <a href="">{{__('messages.certificateTitle3')}}</a>
                                </div>
{{--                                <ul class="category-card__links">--}}
{{--                                    <li><a href="">Generators</a></li>--}}
{{--                                    <li><a href="">Compressors</a></li>--}}
{{--                                    <li><a href="">Winches</a></li>--}}
{{--                                    <li><a href="">Plasma Cutting</a></li>--}}
{{--                                    <li><a href="">Electric Motors</a></li>--}}
{{--                                </ul>--}}
{{--                                <div class="category-card__all">--}}
{{--                                    <a href="">Show All</a>--}}
{{--                                </div>--}}
                                <div class="category-card__products">
                                    {{__('messages.certificateDesc3')}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-categories__item category-card category-card--layout--compact">
                        <div class="category-card__body">
                            <div class="category-card__image">
                                <a href=""><img src="images/categories/category-5.jpg" alt=""></a>
                            </div>
                            <div class="category-card__content">
                                <div class="category-card__name">
                                    <a href="">{{__('messages.certificateTitle5')}}</a>
                                </div>
{{--                                <ul class="category-card__links">--}}
{{--                                    <li><a href="">Tape Measure</a></li>--}}
{{--                                    <li><a href="">Theodolites</a></li>--}}
{{--                                    <li><a href="">Thermal Imagers</a></li>--}}
{{--                                    <li><a href="">Calipers</a></li>--}}
{{--                                    <li><a href="">Levels</a></li>--}}
{{--                                </ul>--}}
{{--                                <div class="category-card__all">--}}
{{--                                    <a href="">Show All</a>--}}
{{--                                </div>--}}
                                <div class="category-card__products">
                                    {{__('messages.certificateDesc5')}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-categories__item category-card category-card--layout--compact">
                        <div class="category-card__body">
                            <div class="category-card__image">
                                <a href=""><img src="images/categories/category-6.jpg" alt=""></a>
                            </div>
                            <div class="category-card__content">
                                <div class="category-card__name">
                                    <a href="">{{__('messages.certificateTitle6')}}</a>
                                </div>
{{--                                <ul class="category-card__links" >--}}
{{--                                    <li><a href="">Winter Workwear</a></li>--}}
{{--                                    <li><a href="">Summer Workwear</a></li>--}}
{{--                                    <li><a href="">Helmets</a></li>--}}
{{--                                    <li><a href="">Belts and Bags</a></li>--}}
{{--                                    <li><a href="">Work Shoes</a></li>--}}
{{--                                </ul>--}}
{{--                                <div class="category-card__all">--}}
{{--                                    <a href="">Show All</a>--}}
{{--                                </div>--}}
                                <div class="category-card__products">
                                    {{__('messages.certificateDesc6')}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .block-categories / end -->
        <!-- .block-features -->
        <div class="block block-features block-features--layout--boxed">
            <div class="container">
                <div class="block-features__list">
                    <div class="block-features__item">
                        <div class="block-features__icon">
                            {{--                            <svg width="48px" height="48px">--}}
                            {{--                                <use xlink:href="images/sprite.svg#fi-free-delivery-48"></use>--}}
                            {{--                            </svg>--}}
                        </div>
                        <div class="block-features__content">
                            <div class="block-features__title">استاندارد بین المللی</div>
                            <div class="block-features__subtitle">گواهینامه های معتبر</div>
                        </div>
                    </div>
                    <div class="block-features__divider"></div>
                    <div class="block-features__item">
                        <div class="block-features__icon">
                            {{--                            <svg width="48px" height="48px">--}}
                            {{--                                <use xlink:href="images/sprite.svg#fi-24-hours-48"></use>--}}
                            {{--                            </svg>--}}
                        </div>
                        <div class="block-features__content">
                            <div class="block-features__title">تیم حرفه ای</div>
                            <div class="block-features__subtitle">پشتیبانی 24 ساعته</div>
                        </div>
                    </div>
                    <div class="block-features__divider"></div>
                    <div class="block-features__item">
                        <div class="block-features__icon">
                            {{--                            <svg width="48px" height="48px">--}}
                            {{--                                <use xlink:href="images/sprite.svg#fi-payment-security-48"></use>--}}
                            {{--                            </svg>--}}
                        </div>
                        <div class="block-features__content">
                            <div class="block-features__title">تضمین کیفیت</div>
                            <div class="block-features__subtitle">با بهترین متریال</div>
                        </div>
                    </div>
                    <div class="block-features__divider"></div>
                    <div class="block-features__item">
                        <div class="block-features__icon">
                            {{--                            <svg width="48px" height="48px">--}}
                            {{--                                <use xlink:href="images/sprite.svg#fi-tag-48"></use>--}}
                            {{--                            </svg>--}}
                        </div>
                        <div class="block-features__content">
                            <div class="block-features__title">طراحی عالی</div>
                            <div class="block-features__subtitle">جلب رضایت مشتری</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .block-features / end -->
        <!-- .block-posts -->
        <div class="block block-posts block-posts--layout--grid-nl" data-layout="grid-nl">
            <div class="container">
                <div class="block-header">
                    <h3 class="block-header__title">{{__('messages.latestNews')}}</h3>
                    <div class="block-header__divider"></div>
                    <div class="block-header__arrows-list">
                        <button class="block-header__arrow block-header__arrow--left" type="button">
                            <svg width="7px" height="11px">
                                <use xlink:href="images/sprite.svg#arrow-rounded-left-7x11"></use>
                            </svg>
                        </button>
                        <button class="block-header__arrow block-header__arrow--right" type="button">
                            <svg width="7px" height="11px">
                                <use xlink:href="images/sprite.svg#arrow-rounded-right-7x11"></use>
                            </svg>
                        </button>
                    </div>
                </div>
                <div class="block-posts__slider">
                    <div class="owl-carousel">
                        <div class="post-card  ">
                            <div class="post-card__image">
                                <a href="">
                                    <img src="images/posts/post-1.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-card__info">
                                <div class="post-card__category">
                                    <a href="">Special Offers</a>
                                </div>
                                <div class="post-card__name">
                                    <a href="">{{__('messages.newsTitle1')}}</a>
                                </div>
                                <div class="post-card__date">{{__('messages.newsDate1')}}</div>
                                <div class="post-card__content">
                                    {{__('messages.newsDesc1')}}
                                </div>
                                <div class="post-card__read-more">
                                    <a href="" class="btn btn-secondary btn-sm">{{__('messages.readMore')}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-card  ">
                            <div class="post-card__image">
                                <a href="">
                                    <img src="images/posts/post-2.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-card__info">
                                <div class="post-card__category">
                                    <a href="">Special Offers</a>
                                </div>
                                <div class="post-card__name">
                                    <a href="">{{__('messages.newsTitle1')}}</a>
                                </div>
                                <div class="post-card__date">{{__('messages.newsDate1')}}</div>
                                <div class="post-card__content">
                                    {{__('messages.newsDesc1')}}
                                </div>
                                <div class="post-card__read-more">
                                    <a href="" class="btn btn-secondary btn-sm">{{__('messages.readMore')}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-card  ">
                            <div class="post-card__image">
                                <a href="">
                                    <img src="images/posts/post-3.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-card__info">
                                <div class="post-card__category">
                                    <a href="">Special Offers</a>
                                </div>
                                <div class="post-card__name">
                                    <a href="">{{__('messages.newsTitle1')}}</a>
                                </div>
                                <div class="post-card__date">{{__('messages.newsDate1')}}</div>
                                <div class="post-card__content">
                                    {{__('messages.newsDesc1')}}
                                </div>
                                <div class="post-card__read-more">
                                    <a href="" class="btn btn-secondary btn-sm">{{__('messages.readMore')}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-card  ">
                            <div class="post-card__image">
                                <a href="">
                                    <img src="images/posts/post-4.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-card__info">
                                <div class="post-card__category">
                                    <a href="">Special Offers</a>
                                </div>
                                <div class="post-card__name">
                                    <a href="">{{__('messages.newsTitle1')}}</a>
                                </div>
                                <div class="post-card__date">{{__('messages.newsDate1')}}</div>
                                <div class="post-card__content">
                                    {{__('messages.newsDesc1')}}
                                </div>
                                <div class="post-card__read-more">
                                    <a href="" class="btn btn-secondary btn-sm">{{__('messages.readMore')}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-card  ">
                            <div class="post-card__image">
                                <a href="">
                                    <img src="images/posts/post-5.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-card__info">
                                <div class="post-card__category">
                                    <a href="">Special Offers</a>
                                </div>
                                <div class="post-card__name">
                                    <a href="">{{__('messages.newsTitle1')}}</a>
                                </div>
                                <div class="post-card__date">{{__('messages.newsDate1')}}</div>
                                <div class="post-card__content">
                                    {{__('messages.newsDesc1')}}
                                </div>
                                <div class="post-card__read-more">
                                    <a href="" class="btn btn-secondary btn-sm">{{__('messages.readMore')}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-card  ">
                            <div class="post-card__image">
                                <a href="">
                                    <img src="images/posts/post-6.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-card__info">
                                <div class="post-card__category">
                                    <a href="">Special Offers</a>
                                </div>
                                <div class="post-card__name">
                                    <a href="">{{__('messages.newsTitle1')}}</a>
                                </div>
                                <div class="post-card__date">{{__('messages.newsDate1')}}</div>
                                <div class="post-card__content">
                                    {{__('messages.newsDesc1')}}
                                </div>
                                <div class="post-card__read-more">
                                    <a href="" class="btn btn-secondary btn-sm">{{__('messages.readMore')}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-card  ">
                            <div class="post-card__image">
                                <a href="">
                                    <img src="images/posts/post-7.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-card__info">
                                <div class="post-card__category">
                                    <a href="">Special Offers</a>
                                </div>
                                <div class="post-card__name">
                                    <a href="">{{__('messages.newsTitle1')}}</a>
                                </div>
                                <div class="post-card__date">{{__('messages.newsDate1')}}</div>
                                <div class="post-card__content">
                                    {{__('messages.newsDesc1')}}
                                </div>
                                <div class="post-card__read-more">
                                    <a href="" class="btn btn-secondary btn-sm">{{__('messages.readMore')}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-card  ">
                            <div class="post-card__image">
                                <a href="">
                                    <img src="images/posts/post-8.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-card__info">
                                <div class="post-card__category">
                                    <a href="">Special Offers</a>
                                </div>
                                <div class="post-card__name">
                                    <a href="">{{__('messages.newsTitle1')}}</a>
                                </div>
                                <div class="post-card__date">{{__('messages.newsDate1')}}</div>
                                <div class="post-card__content">
                                    {{__('messages.newsDesc1')}}
                                </div>
                                <div class="post-card__read-more">
                                    <a href="" class="btn btn-secondary btn-sm">{{__('messages.readMore')}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-card  ">
                            <div class="post-card__image">
                                <a href="">
                                    <img src="images/posts/post-9.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-card__info">
                                <div class="post-card__category">
                                    <a href="">Special Offers</a>
                                </div>
                                <div class="post-card__name">
                                    <a href="">{{__('messages.newsTitle1')}}</a>
                                </div>
                                <div class="post-card__date">{{__('messages.newsDate1')}}</div>
                                <div class="post-card__content">
                                    {{__('messages.newsDesc1')}}
                                </div>
                                <div class="post-card__read-more">
                                    <a href="" class="btn btn-secondary btn-sm">{{__('messages.readMore')}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-card  ">
                            <div class="post-card__image">
                                <a href="">
                                    <img src="images/posts/post-10.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-card__info">
                                <div class="post-card__category">
                                    <a href="">Special Offers</a>
                                </div>
                                <div class="post-card__name">
                                    <a href="">{{__('messages.newsTitle1')}}</a>
                                </div>
                                <div class="post-card__date">{{__('messages.newsDate1')}}</div>
                                <div class="post-card__content">
                                    {{__('messages.newsDesc1')}}
                                </div>
                                <div class="post-card__read-more">
                                    <a href="" class="btn btn-secondary btn-sm">{{__('messages.readMore')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .block-posts / end -->
        <!-- .block-brands -->
        <div class="block block-brands">
            <div class="container">
                <div class="block-brands__slider">
                    <div class="owl-carousel">
                        <div class="block-brands__item">
                            <a href=""><img src="images/logos/logo-4.png" alt=""></a>
                        </div>
                        <div class="block-brands__item">
                            <a href=""><img src="images/logos/logo-4.png" alt=""></a>
                        </div>
                        <div class="block-brands__item">
                            <a href=""><img src="images/logos/logo-4.png" alt=""></a>
                        </div>
                        <div class="block-brands__item">
                            <a href=""><img src="images/logos/logo-4.png" alt=""></a>
                        </div>
                        <div class="block-brands__item">
                            <a href=""><img src="images/logos/logo-4.png" alt=""></a>
                        </div>
                        <div class="block-brands__item">
                            <a href=""><img src="images/logos/logo-4.png" alt=""></a>
                        </div>
                        <div class="block-brands__item">
                            <a href=""><img src="images/logos/logo-4.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .block-brands / end -->
    </div>
    <!-- site__body / end -->
    <!-- site__footer -->
    <footer class="site__footer">
        <div class="site-footer">
            <div class="container">
                <div class="site-footer__widgets">
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="site-footer__widget footer-contacts">
                                <h5 class="footer-contacts__title">{{__('messages.contactUs')}}</h5>
                                <ul class="footer-contacts__contacts">
                                    <li><i class="footer-contacts__icon fas fa-globe-americas"></i>{{__('addressInfo.address')}}</li>
                                    <li><i class="footer-contacts__icon far fa-envelope"></i> {{__('addressInfo.email')}}</li>
                                    <li><i class="footer-contacts__icon fas fa-mobile-alt"></i> {{__('addressInfo.phone')}}</li>
                                    <li><i class="footer-contacts__icon far fa-clock"></i> {{__('addressInfo.workingHour1')}}</li>
                                    <li><i class="footer-contacts__icon far fa-clock"></i> {{__('addressInfo.workingHour2')}}</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-6 col-md-3 col-lg-2">
                            <div class="site-footer__widget footer-links">
                                <h5 class="footer-links__title">{{__('messages.information')}}</h5>
                                <ul class="footer-links__list">
                                    <li class="footer-links__item"><a href="" class="footer-links__link">{{__('messages.news')}}</a></li>
                                    <li class="footer-links__item"><a href="" class="footer-links__link">{{__('messages.products')}}</a></li>
                                    <li class="footer-links__item"><a href="" class="footer-links__link">{{__('messages.certificates')}}</a></li>
                                    <li class="footer-links__item"><a href="" class="footer-links__link">{{__('messages.galleries')}}</a></li>
                                    <li class="footer-links__item"><a href="" class="footer-links__link">{{__('messages.employment')}}</a></li>
                                    <li class="footer-links__item"><a href="" class="footer-links__link">{{__('messages.contactUs')}}</a></li>
                                    <li class="footer-links__item"><a href="" class="footer-links__link">{{__('messages.aboutUs')}}</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-6 col-md-3 col-lg-2">
                            <div class="site-footer__widget footer-links">
                                <h5 class="footer-links__title">{{__('messages.products')}}</h5>
                                <ul class="footer-links__list">
                                    <li class="footer-links__item"><a href="" class="footer-links__link">{{__('messages.squareBoxProfile')}}</a></li>
                                    <li class="footer-links__item"><a href="" class="footer-links__link">{{__('messages.rectangularBoxProfile')}}</a></li>
                                    <li class="footer-links__item"><a href="" class="footer-links__link">{{__('messages.windowProfile')}}</a></li>
                                    <li class="footer-links__item"><a href="" class="footer-links__link">{{__('messages.industPipe')}}r</a></li>
                                    <li class="footer-links__item"><a href="" class="footer-links__link">{{__('messages.gasPipe')}}</a></li>
                                    <li class="footer-links__item"><a href="" class="footer-links__link">{{__('messages.zProfile')}}</a></li>
                                    <li class="footer-links__item"><a href="" class="footer-links__link">{{__('messages.sheet')}}</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-4">
                            <div class="site-footer__widget footer-newsletter">
                                <h5 class="footer-newsletter__title">{{__('messages.arashProfile')}}</h5>

                                <div class="footer-contacts__text">
                                    {{__('messages.manufactureDescFooter')}}
                                </div>

                                <div class="footer-newsletter__text footer-newsletter__text--social">
                                    {{__('messages.socialMediaDesc')}}
                                </div>

                                <ul class="footer-newsletter__social-links">
                                    <li class="footer-newsletter__social-link footer-newsletter__social-link--facebook"><a href="/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                    <li class="footer-newsletter__social-link footer-newsletter__social-link--twitter"><a href="/" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                    <li class="footer-newsletter__social-link footer-newsletter__social-link--youtube"><a href="/" target="_blank"><i class="fab fa-youtube"></i></a></li>
                                    <li class="footer-newsletter__social-link footer-newsletter__social-link--instagram"><a href="/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                    <li class="footer-newsletter__social-link footer-newsletter__social-link--rss"><a href="/" target="_blank"><i class="fas fa-rss"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="site-footer__bottom">
                    <div class="site-footer__copyright">
                        {{__('messages.poweredBy')}} {{__('messages.arashProfile')}} — {{__('messages.designBy')}} <a href="/" target="_blank">{{__('messages.arashProfile')}}</a>
                    </div>
                    <div class="site-footer__payments">
                        <img src="images/payments.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- site__footer / end -->
</div>
<!-- site / end -->
</body>

</html>
