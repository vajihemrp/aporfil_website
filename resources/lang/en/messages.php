<?php
return [
    'welcome' => 'Welcome to our application!',
    'products' => 'Products',
    'language'=> 'Language',
    'currency' => 'Currency',
    'certificates' => 'Certificates',
    'latestNews'=> 'Latest News',
    'galleries'=>'Galleries',
    'menu'=>'Menu',
    'news'=>'News',
    'employment'=>'Employment',
    'searchMsg'=> 'Search over products',
    'profile'=>'Profile',
    'pipe'=> 'Pipe',
    'sheet'=>'Sheet',
    'squareBoxProfile'=>'Square Box Profile',
    'rectangularBoxProfile'=>'Rectangular Box Profiles',
    'windowProfile'=>'Window Profile',
    'industPipe'=>'Metal Pipe',
    'gasPipe'=>'Gas Pipe',
    'zProfile'=>'Z-Profile',
    'aboutShortMsg'=> 'In one general sense, philosophy is associated with wisdom, intellectual culture and a search for knowledge. In that sense, all cultures... ',
    'aboutUs'=>'About Us',
    'readMore'=>'Read More',
    'aboutUsTitle' => 'About Us',
    'size'=> 'Size',
    'thickness' =>'Thickness',
    'certificateTitle1'=> 'Certificate Title 1',
    'certificateDesc1'=>  'Certificate Description 1',
    'certificateTitle2'=> 'Certificate Title 2',
    'certificateDesc2'=> 'Certificate Description 2',
    'certificateTitle3'=> 'Certificate Title 3',
    'certificateDesc3'=>  'Certificate Description 3',
    'certificateTitle4'=> 'Certificate Title 4',
    'certificateDesc4'=>  'Certificate Description 4',
    'certificateTitle5'=> 'Certificate Title 5',
    'certificateDesc5'=>  'Certificate Description 5',
    'certificateTitle6'=> 'Certificate Title 6',
    'certificateDesc6'=>  'Certificate Description 6',
    'newsTitle1'=> 'Lorem ipsum dolor sit amet',
    'newsDesc1'=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...',
    'newsDate1'=> 'October 19, 2019',
    'contactUs'=> 'Contact Us',
    'information' => 'Information',
    'arashProfile' => 'Arash Profile',
    'manufactureDescFooter'=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in feugiat lorem. Pellentque ac placerat tellus. ',
    'poweredBy'=>'Powered by',
    'designBy'=> 'Design by',
    'socialMediaDesc'=> 'Follow us on social networks'
];
